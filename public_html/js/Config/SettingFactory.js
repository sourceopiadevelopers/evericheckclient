(function () {
    /* Setup global settings */
    angular.module('evericheck').factory('settings', ['$rootScope','$window', function ($rootScope, $window) {
            
            var settings = {
                layout: {
                    pageSidebarClosed: false, // sidebar menu state
                    pageContentWhite: true, // set page content layout
                    pageBodySolid: false, // solid body color state
                    pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
                },
                assetsPath: '../assets',
                globalPath: '../assets/global',
                layoutPath: '../assets/layouts/layout3',
                baseUrl:'http://localhost/EvericheckAPI'
                
            };
            
            if ($window.localStorage.getItem('LoggedInUser') !== null)
            {
                var user = JSON.parse($window.localStorage.getItem('LoggedInUser'));
                settings.username = user.name;
                settings.password = user.password;
            }
                        
            return settings;
        }]);
})();