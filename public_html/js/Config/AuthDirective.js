(function(){
    
    function AuthDirective($rootScope, $location){
        return {
            replace:true,
            template:"",
            link: function(scope, element, attrs, ngModel) {
                scope.$on('event:auth-loginRequired', function(){
                    $location.path('/login');
                    console.log('auth required');
                });
            }
        };
    }
    
    AuthDirective.$inject = ["$rootScope","$location"];
    
    angular.module('evericheck').directive("authenticateApp", AuthDirective);
})();

