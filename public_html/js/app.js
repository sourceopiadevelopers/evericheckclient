(function () {

    "use strict";
    angular.module("evericheck", [
        "ui.router",
        "ui.bootstrap",
        "oc.lazyLoad",
        "ngSanitize",
        "http-auth-interceptor",
        "angularUtils.directives.dirPagination",
        "angularValidator"
    ]);

    /* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
    angular.module("evericheck").config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
            $ocLazyLoadProvider.config({
                // global configs go here
            });
        }]);

    //AngularJS v1.3.x workaround for old style controller declarition in HTML
    angular.module("evericheck").config(['$controllerProvider', function ($controllerProvider) {
            // this option might be handy for migrating old apps, but please don't use it
            // in new ones!
            $controllerProvider.allowGlobals();
        }]);

    


    /* Setup App Main Controller */
    angular.module("evericheck").controller('AppController', ['$scope', '$rootScope', function ($scope, $rootScope) {
            $scope.$on('$viewContentLoaded', function () {
                App.initComponents(); // init core components
                //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive
            });
//            console.log(process.env.evericheck_core_debug);
        }]);


    /***
     Layout Partials.
     By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial
     initialization can be disabled and Layout.init() should be called on page load complete as explained above.
     ***/

    /* Setup Layout Part - Header */
    angular.module("evericheck").controller('HeaderController', ['$scope', function ($scope) {
            $scope.$on('$includeContentLoaded', function () {
                Layout.initHeader(); // init header
            });
        }]);

    /* Setup Layout Part - Sidebar */
    angular.module("evericheck").controller('SidebarController', ['$scope', function ($scope) {
            $scope.$on('$includeContentLoaded', function () {
                Layout.initSidebar(); // init sidebar
            });
        }]);

    /* Setup Layout Part - Quick Sidebar */
    angular.module("evericheck").controller('QuickSidebarController', ['$scope', function ($scope) {
            $scope.$on('$includeContentLoaded', function () {
                setTimeout(function () {
                    QuickSidebar.init(); // init quick sidebar
                }, 2000)
            });
        }]);

    /* Setup Layout Part - Sidebar */
    angular.module("evericheck").controller('PageHeadController', ['$scope', function ($scope) {
            $scope.$on('$includeContentLoaded', function () {
                Demo.init(); // init theme panel
            });
        }]);

    /* Setup Layout Part - Theme Panel */
    angular.module("evericheck").controller('ThemePanelController', ['$scope', function ($scope) {
            $scope.$on('$includeContentLoaded', function () {
                Demo.init(); // init theme panel
            });
        }]);

    /* Setup Layout Part - Footer */
    angular.module("evericheck").controller('FooterController', ['$scope', function ($scope) {
            $scope.$on('$includeContentLoaded', function () {
                Layout.initFooter(); // init footer
            });
        }]);



    angular.module("evericheck").config(['$stateProvider', '$urlRouterProvider',
        function ($stateProvider, $urlRouterProvider) {

            $urlRouterProvider.otherwise("/");

            $stateProvider

                    .state('login', {
                        url: "/",
                        data: {pageTitle: 'Login'},
                        templateUrl: "views/login.html",
                        controller: "LoginController",
                        resolve: {
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                    return $ocLazyLoad.load({
                                        name: 'evericheck',
                                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                                        files: [
                                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                                            '../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                                            '../assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css',
                                            '../assets/global/plugins/typeahead/typeahead.css',
                                            '../assets/global/plugins/fuelux/js/spinner.min.js',
                                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                                            '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                                            '../assets/global/plugins/jquery.input-ip-address-control-1.0.min.js',
                                            '../assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js',
                                            '../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
                                            '../assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js',
                                            '../assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js',
                                            '../assets/global/plugins/typeahead/handlebars.min.js',
                                            '../assets/pages/scripts/components-form-tools-2.min.js',
                                            'js/Services/LoginFactory.js',
                                            'js/Controller/LoginController.js'
                                        ]
                                    });
                                }]
                        }
                    })

                    .state('theme', {
                        templateUrl: "tpl/master.html"
                    })

                    .state('theme.dashboard', {
                        url: "^/dashboard",
                        templateUrl: "views/dashboard.html",
                        data: {pageTitle: 'Dashboard'},
                        controller: "DashboardController",
                        resolve: {
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                    return $ocLazyLoad.load({
                                        name: 'evericheck',
                                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                                        files: [
                                            '../assets/global/plugins/morris/morris.css',
                                            '../assets/global/plugins/morris/morris.min.js',
                                            '../assets/global/plugins/morris/raphael-min.js',
                                            '../assets/global/plugins/jquery.sparkline.min.js',
                                            '../assets/pages/scripts/dashboard.min.js',
                                            'js/Services/DashboardFactory.js',
                                            'js/Controller/DashboardController.js'
                                        ]
                                    });
                                }]
                        }
                    })



                    .state('theme.merchants/merchantCurrentMonthActivity', {
                        url: "^/reports/getMerchantCurrentMonthlyActivity",
                        templateUrl: "views/Merchants/merchantCurrentMonthActivity.html",
                        data: {pageTitle: 'Merchants Current Month Activity'},
                        controller: "MerchantController",
                        resolve: {
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                    return $ocLazyLoad.load({
                                        name: 'Evericheck',
                                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                                        files: [
                                            '../assets/global/plugins/datatables/datatables.min.css',
                                            '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                                            '../assets/global/plugins/datatables/datatables.all.min.js',
                                            '../assets/pages/scripts/table-datatables-managed.min.js',
                                            'js/Services/MerchantFactory.js',
                                            'js/Controller/MerchantController.js',
//                                            'js/Controller/PaginationController.js'
                                            
                                        ]
                                    });
                                }]
                        }
                    })
                    .state('theme.reports/batchesByDate', {
                        url: "^/reports/batchesByDate",
                        templateUrl: "views/Reports/batchesByDate.html",
                        data: {pageTitle: 'Batches'},
                        controller: "ReportController",
                        resolve: {
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                    return $ocLazyLoad.load({
                                        name: 'evericheck',
                                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                                        files: [
                                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                                            '../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
                                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                                            '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
                                            '../assets/global/plugins/bootstrap-daterangepicker/moment.min.js',
                                            '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js',
                                            '../assets/pages/scripts/components-date-time-pickers.min.js',
                                            '../assets/global/plugins/datatables/datatables.min.css',
                                            '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                                            '../assets/global/plugins/datatables/datatables.all.min.js',
                                            '../assets/pages/scripts/table-datatables-managed.min.js',
                                            '../assets/pages/scripts/components-form-tools-2.min.js',
                                            'js/Services/MerchantActivityFactory.js',
                                            'js/Services/IsoActivityFactory.js',
                                            'js/Controller/ReportController.js',
                                            'js/Services/ReportFactory.js',
                                            'js/Services/BatchFactory.js',
                                        ]
                                    });
                                }]
                        }
                    })


                    .state('theme.reports/isosCurrentMonthActivity', {
                        url: "^/reports/isosCurrentMonthActivity",
                        templateUrl: "views/Reports/isosCurrentMonthActivity.html",
                        data: {pageTitle: 'ISOs Current Month Activity'},
                        controller: "ReportController",
                        resolve: {
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                    return $ocLazyLoad.load({
                                        name: 'evericheck',
                                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                                        files: [
                                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                                            '../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                                            '../assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css',
                                            '../assets/global/plugins/typeahead/typeahead.css',
                                            '../assets/global/plugins/datatables/datatables.min.css',
                                            '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                                            '../assets/global/plugins/datatables/datatables.all.min.js',
                                            '../assets/pages/scripts/table-datatables-managed.min.js',
                                            '../assets/global/plugins/fuelux/js/spinner.min.js',
                                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js',
                                            '../assets/global/plugins/jquery-inputmask/jquery.inputmask.bundle.min.js',
                                            '../assets/global/plugins/jquery.input-ip-address-control-1.0.min.js',
                                            '../assets/global/plugins/bootstrap-pwstrength/pwstrength-bootstrap.min.js',
                                            '../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
                                            '../assets/global/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js',
                                            '../assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.js',
                                            '../assets/global/plugins/typeahead/handlebars.min.js',
                                            '../assets/pages/scripts/components-form-tools-2.min.js',
                                            'js/Services/IsoActivityFactory.js',
                                            'js/Services/MerchantActivityFactory.js',
                                            'js/Controller/ReportController.js',
                                            'js/Services/ReportFactory.js',
                                            'js/Services/BatchFactory.js'

                                        ]
                                    });
                                }]
                        }
                    })
                    .state('theme.reports/monthlyMerchantActivity', {
                        url: "^/reports/monthlyMerchantActivity",
                        templateUrl: "views/Reports/monthlyMerchantActivity.html",
                        data: {pageTitle: 'Merchant Monthly Activity'},
                        controller: "ReportController",
                        resolve: {
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                    return $ocLazyLoad.load({
                                        name: 'evericheck',
                                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                                        files: [
                                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                                            '../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
                                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                                            '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
                                            '../assets/global/plugins/moment.min.js',
                                            '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js',
                                            '../assets/global/plugins/datatables/datatables.min.css',
                                            '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                                            '../assets/global/plugins/datatables/datatables.all.min.js',
                                            '../assets/pages/scripts/table-datatables-managed.min.js',
                                            '../assets/pages/scripts/components-form-tools-2.min.js',
                                            'js/Controller/ReportController.js',
                                            'js/Services/ReportFactory.js',
                                            'js/Services/BatchFactory.js',
                                            'js/Services/IsoActivityFactory.js',
                                            'js/Services/MerchantActivityFactory.js'
                                        ]
                                    });
                                }]
                        }
                    })

                    .state('theme.reports/monthlyIsoActivity', {
                        url: "^/reports/monthlyIsoActivity",
                        templateUrl: "views/Reports/monthlyIsoActivity.html",
                        data: {pageTitle: 'ISO Monthly Activity'},
                        controller: "ReportController",
                        resolve: {
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                    return $ocLazyLoad.load({
                                        name: 'evericheck',
                                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                                        files: [
                                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                                            '../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
                                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                                            '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
                                            '../assets/global/plugins/moment.min.js',
                                            '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js',
                                            '../assets/pages/scripts/components-date-time-pickers.min.js',
                                            '../assets/global/plugins/datatables/datatables.min.css',
                                            '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                                            '../assets/global/plugins/datatables/datatables.all.min.js',
                                            '../assets/pages/scripts/table-datatables-managed.min.js',
                                            '../assets/pages/scripts/components-form-tools-2.min.js',
                                            'js/Controller/ReportController.js',
                                            'js/Services/ReportFactory.js',
                                            'js/Services/BatchFactory.js',
                                            'js/Services/MerchantActivityFactory.js',
                                            'js/Services/IsoActivityFactory.js'

                                        ]
                                    });
                                }]
                        }
                    })


                    .state('theme.reports/isoMerchantsCurrentMonthActivity', {
                        url: "^/reports/isoMerchantsCurrentMonthActivity",
                        templateUrl: "views/Reports/isoMerchantsCurrentMonthActivity.html",
                        data: {pageTitle: 'ISO Merchants Current Month Activity'},
                        controller: "ReportController",
                        resolve: {
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                    return $ocLazyLoad.load({
                                        name: 'evericheck',
                                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                                        files: [
                                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                                            '../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
                                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                                            '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
                                            '../assets/global/plugins/moment.min.js',
                                            '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js',
                                            '../assets/pages/scripts/components-date-time-pickers.min.js',
                                            '../assets/global/plugins/datatables/datatables.min.css',
                                            '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                                            '../assets/global/plugins/datatables/datatables.all.min.js',
                                            '../assets/pages/scripts/table-datatables-managed.min.js',
                                            '../assets/pages/scripts/components-form-tools-2.min.js',
                                            'js/Controller/ReportController.js',
                                            'js/Services/ReportFactory.js',
                                            'js/Services/BatchFactory.js',
                                            'js/Services/MerchantActivityFactory.js',
                                            'js/Services/IsoActivityFactory.js'
                                        ]
                                    });
                                }]
                        }
                    })


                      .state('theme.checkers/checkBatchesCount', {
                        url: "^/checkers/checkBatchesCount",
                        templateUrl: "views/Checkers/checkBatchesCount.html",
                        data: {pageTitle: 'Batches Count Checker'},
                        controller: "CheckersController",
                        resolve: {
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                    return $ocLazyLoad.load({
                                        name: 'evericheck',
                                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                                        files: [
                                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                                            '../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
                                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                                            '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
                                            '../assets/global/plugins/moment.min.js',
                                            '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js',
                                            '../assets/pages/scripts/components-date-time-pickers.min.js',
                                            '../assets/global/plugins/datatables/datatables.min.css',
                                            '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                                            '../assets/global/plugins/datatables/datatables.all.min.js',
                                            '../assets/pages/scripts/table-datatables-managed.min.js',
                                            '../assets/pages/scripts/components-form-tools-2.min.js',
                                            '../assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css',
                            '../assets/global/plugins/select2/css/select2.min.css',
                            '../assets/global/plugins/select2/css/select2-bootstrap.min.css',

                            '../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js',
                            '../assets/global/plugins/select2/js/select2.full.min.js',

                            '../assets/pages/scripts/components-bootstrap-select.min.js',
                            '../assets/pages/scripts/components-select2.min.js',
                                            'js/Controller/CheckersController.js',
                                            'js/Controller/ReportController.js',
                                            'js/Services/ReportFactory.js',
                                            'js/Services/BatchFactory.js',
                                            'js/Services/MerchantActivityFactory.js',
                                            'js/Services/IsoActivityFactory.js'
                                        ]
                                    });
                                }]
                        }
                    })
                    
                    .state('theme.checkers/batchVsAllMerchantsWithinABatchCount', {
                        url: "^/checkers/batchVsAllMerchantsWithinABatchCount",
                        templateUrl: "views/Checkers/batchVsAllMerchantsWithinABatchCount.html",
                        data: {pageTitle: 'Batch Detail Vs All Merchants Within A Batch'},
                        controller: "CheckersController",
                        resolve: {
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                    return $ocLazyLoad.load({
                                        name: 'evericheck',
                                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                                        files: [
                                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                                            '../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
                                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                                            '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
                                            '../assets/global/plugins/moment.min.js',
                                            '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js',
                                            '../assets/pages/scripts/components-date-time-pickers.min.js',
                                            '../assets/global/plugins/datatables/datatables.min.css',
                                            '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                                            '../assets/global/plugins/datatables/datatables.all.min.js',
                                            '../assets/pages/scripts/table-datatables-managed.min.js',
                                            '../assets/pages/scripts/components-form-tools-2.min.js',
                                            '../assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css',
                            '../assets/global/plugins/select2/css/select2.min.css',
                            '../assets/global/plugins/select2/css/select2-bootstrap.min.css',

                            '../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js',
                            '../assets/global/plugins/select2/js/select2.full.min.js',

                            '../assets/pages/scripts/components-bootstrap-select.min.js',
                            '../assets/pages/scripts/components-select2.min.js',
                                            'js/Controller/CheckersController.js',
                                            'js/Controller/ReportController.js',
                                            'js/Services/ReportFactory.js',
                                            'js/Services/BatchFactory.js',
                                            'js/Services/MerchantActivityFactory.js',
                                            'js/Services/IsoActivityFactory.js'
                                        ]
                                    });
                                }]
                        }
                    })
                    
                     .state('theme.checkers/batchVsMerchantMonthlyActivity', {
                        url: "^/checkers/batchVsMerchantMonthlyActivity",
                        templateUrl: "views/Checkers/batchVsMerchantMonthlyActivity.html",
                        data: {pageTitle: 'Batch vs Merchant Monthly Activity'},
                        controller: "CheckersController",
                        resolve: {
                            deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                                    return $ocLazyLoad.load({
                                        name: 'evericheck',
                                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                                        files: [
                                            '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                                            '../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                                            '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
                                            '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                                            '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
                                            '../assets/global/plugins/moment.min.js',
                                            '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js',
                                            '../assets/pages/scripts/components-date-time-pickers.min.js',
                                            '../assets/global/plugins/datatables/datatables.min.css',
                                            '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                                            '../assets/global/plugins/datatables/datatables.all.min.js',
                                            '../assets/pages/scripts/table-datatables-managed.min.js',
                                            '../assets/pages/scripts/components-form-tools-2.min.js',
                                            '../assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css',
                            '../assets/global/plugins/select2/css/select2.min.css',
                            '../assets/global/plugins/select2/css/select2-bootstrap.min.css',

                            '../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js',
                            '../assets/global/plugins/select2/js/select2.full.min.js',

                            '../assets/pages/scripts/components-bootstrap-select.min.js',
                            '../assets/pages/scripts/components-select2.min.js',
                                            'js/Controller/CheckersController.js',
                                            'js/Controller/ReportController.js',
                                            'js/Services/ReportFactory.js',
                                            'js/Services/BatchFactory.js',
                                            'js/Services/MerchantActivityFactory.js',
                                            'js/Services/IsoActivityFactory.js'
                                        ]
                                    });
                                }]
                        }
                    })

                .state('theme.checkers/batchVsIsoMonthlyActivity', {
                    url: "^/checkers/batchVsIsoMonthlyActivity",
                    templateUrl: "views/Checkers/batchVsIsoMonthlyActivity.html",
                    data: {pageTitle: 'Batch vs Iso Monthly Activity'},
                    controller: "CheckersController",
                    resolve: {
                        deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                            return $ocLazyLoad.load({
                                name: 'evericheck',
                                insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                                files: [
                                    '../assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css',
                                    '../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
                                    '../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css',
                                    '../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js',
                                    '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css',
                                    '../assets/global/plugins/moment.min.js',
                                    '../assets/global/plugins/bootstrap-daterangepicker/daterangepicker.js',
                                    '../assets/pages/scripts/components-date-time-pickers.min.js',
                                    '../assets/global/plugins/datatables/datatables.min.css',
                                    '../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                                    '../assets/global/plugins/datatables/datatables.all.min.js',
                                    '../assets/pages/scripts/table-datatables-managed.min.js',
                                    '../assets/pages/scripts/components-form-tools-2.min.js',
                                    '../assets/global/plugins/bootstrap-select/css/bootstrap-select.min.css',
                                    '../assets/global/plugins/select2/css/select2.min.css',
                                    '../assets/global/plugins/select2/css/select2-bootstrap.min.css',

                                    '../assets/global/plugins/bootstrap-select/js/bootstrap-select.min.js',
                                    '../assets/global/plugins/select2/js/select2.full.min.js',

                                    '../assets/pages/scripts/components-bootstrap-select.min.js',
                                    '../assets/pages/scripts/components-select2.min.js',
                                    'js/Controller/CheckersController.js',
                                    'js/Controller/ReportController.js',
                                    'js/Services/ReportFactory.js',
                                    'js/Services/BatchFactory.js',
                                    'js/Services/MerchantActivityFactory.js',
                                    'js/Services/IsoActivityFactory.js'
                                ]
                            });
                        }]
                    }
                })



                    .state('logout', {
                        url: '/logout',
                        controller: function ($window, $state) {
                            delete $window.localStorage.clear();
                            $state.transitionTo("login", {}, {inherit: false, reload: true, notify: true});
                        }
                    });


        }]);

    angular.module('evericheck').run(["$rootScope", "settings", "$state", function ($rootScope, settings, $state) {
            $rootScope.state = $state; // state to be accessed from view
            $rootScope.settings = settings; // state to be accessed from view
        }]);


})();



