/***
 GLobal Directives
 ***/

// Route State Load Spinner(used on page or content load)
angular.module('evericheck').directive('ngSpinnerBar', ['$rootScope', 'settings',
    function ($rootScope, settings) {
        return {
            link: function (scope, element, attrs) {
                // by defult hide the spinner bar
                element.addClass('hide'); // hide spinner bar by default

                // display the spinner bar whenever the route changes(the content part started loading)
                $rootScope.$on('$stateChangeStart', function () {
                    element.removeClass('hide'); // show spinner bar
                    Layout.closeMainMenu();
                });

                // hide the spinner bar on rounte change success(after the content loaded)
                $rootScope.$on('$stateChangeSuccess', function () {
                    element.addClass('hide'); // hide spinner bar
                    $('body').removeClass('page-on-load'); // remove page loading indicator
                    Layout.setMainMenuActiveLink('match'); // activate selected link in the sidebar menu

                    // auto scorll to page top
                    setTimeout(function () {
                        App.scrollTop(); // scroll to the top on content load
                    }, settings.layout.pageAutoScrollOnLoad);
                });

                // handle errors
                $rootScope.$on('$stateNotFound', function () {
                    element.addClass('hide'); // hide spinner bar
                });

                // handle errors
                $rootScope.$on('$stateChangeError', function () {
                    element.addClass('hide'); // hide spinner bar
                });
            }
        };
    }
]);

// Handle global LINK click
angular.module('evericheck').directive('a',
        function () {
            return {
                restrict: 'E',
                link: function (scope, elem, attrs) {
                    if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
                        elem.on('click', function (e) {
                            e.preventDefault(); // prevent link click for above criteria
                        });
                    }
                }
            };
        });

// Handle Dropdown Hover Plugin Integration
angular.module('evericheck').directive('dropdownMenuHover', function () {
    return {
        link: function (scope, elem) {
            elem.dropdownHover();
        }
    };
});


angular.module('evericheck').directive('bootstrapSwitch', [
    function () {
        return {
            restrict: 'A',
            require: '?ngModel',
            link: function (scope, element, attrs, ngModel) {
                element.bootstrapSwitch();

                element.on('switchChange.bootstrapSwitch', function (event, state) {
                    if (ngModel) {
                        scope.$apply(function () {
                            ngModel.$setViewValue(state);
                        });
                    }
                });

                scope.$watch(attrs.ngModel, function (newValue, oldValue) {
                    if (newValue) {
                        element.bootstrapSwitch('state', true, true);
                    } else {
                        element.bootstrapSwitch('state', false, true);
                    }
                });
            }
        };
    }
]);
angular.module('evericheck').directive('equals',function(){
    return {
            restrict: 'A', // only activate on element attribute
            require: '?ngModel', // get a hold of NgModelController
            link: function (scope, elem, attrs, ngModel) {
                if (!ngModel) return; // do nothing if no ng-model

                // watch own value and re-validate on change
                scope.$watch(attrs.ngModel, function () {
                    validate();
                });

                // observe the other value and re-validate on change
                attrs.$observe('equals', function (val) {
                    validate();
                });

                var validate = function () {
                    // values
                    var password1 = ngModel.$viewValue;
                    var password2 = attrs.equals;

                    // set validity
                    ngModel.$setValidity('equals', !password1 || !password2 || password1 === password2);
                };
            }
        };
});
angular.module('evericheck').directive('datetimepicker', function () {

    return {
        require: '?ngModel',
        restrict: 'A',
        link: function (scope, element, attrs, ngModel) {
            if (!ngModel)
                return; // do nothing if no ng-model


            function cbMonthly(start, end) {
                element.find('input').attr('value', start.format('YYYY-MM') + ' - ' + end.format('YYYY-MM'));
                var value = element.find('input').val();
                ngModel.$setViewValue(value);
            }

            function cb(start, end) {
                element.find('input').attr('value', start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
                var value = element.find('input').val();
                ngModel.$setViewValue(value);
            }

            element.find('input').on('change', function () {
                var value = element.find('input').val();
                ngModel.$setViewValue(value);
            });


            if (element.attr('id') === 'batchesDatePicker') {

                cb(moment().subtract(29, 'days'), moment());


                element.daterangepicker({
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment(),
                    ranges: {
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'Last 90 Days': [moment().subtract(89, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
                    }

                }, cb);
            } else
            {
                cbMonthly(moment().subtract(6, 'month').startOf('month'), moment().startOf('month'));

                element.daterangepicker({
                    startDate: moment().subtract(6, 'month').startOf('month'),
                    endDate: moment().startOf('month'),
                    showDropdowns: true,
                    ranges: {
                        'Last 6 Months': [moment().subtract(6, 'month').startOf('month'), moment().startOf('month')],
                        'Last 12 Months': [moment().subtract(12, 'month').startOf('month'), moment().startOf('month')],
                        'Last 24 Months': [moment().subtract(24, 'month').startOf('month'), moment().startOf('month')]
                    },
                }, cbMonthly);

            }


            ngModel.$render = function () {
                element.find('input').val(ngModel.$viewValue || '');
            };
        }

    };


});
