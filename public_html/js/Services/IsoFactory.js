(function(){

    function IsoFactory($http, $window, settings) {
        return {
            fetch: function (successCallback, errorCallback) {
                    $http({
                        method: 'GET',
                        url: settings.baseUrl + '/reports/getIsos',
                        useXDomain: true,
                        headers: {'Authorization': $window.localStorage.getItem('Authorization')}
                    })
                        .success(function (data) {
                            successCallback(data);
                        })
                        .error(function (data) {
                            errorCallback(data);
                        });
            },
            fetchById: function (isoId, successCallback, errorCallback) {
                $http({
                        method: 'GET',
                        url: settings.baseUrl + '/reports/getIsosById',
                        params: isoId,
                        headers: {'Authorization': $window.localStorage.getItem('Authorization')}
                    })
                        .success(function (data) {
                            successCallback(data);
                        })
                        .error(function (data) {
                            errorCallback(data);
                        });
            }
        };
    }

    IsoFactory.$inject = ['$http', '$window', 'settings'];

    angular.module('evericheck').factory('IsoFactory', IsoFactory);

})();