(function () {


    function ReportFactory($http, $window, settings) {
        var report1 = [
            {
                "IsoNumber": 7800,
                "IsoActivityHistory": "history",
                "IsoMerchantActivity": "merchants",
                "IsoName": "1st choice Business Services Inc",
                "ActiveofMerchants": 2,
                "NewofMerchantsMTD": 0,
                "ClosedofMerchantsMTD": 0,
                "ofTransLastMTD": 6,
                "ofTransThisMTD": 5,
                "TransVariancepercentage": -16.67,
                "SumAmountLastMTD": 720,
                "SumAmountThisMTD": 420,
                "SumAmountVariancepercentage": -41.67,
                "ofTransLastMonth": 10,
                "SumAmountLastMonth": 1150,
                "ofTransYTD": 5,
                "SumAmountYTD": 420
            },
            {
                "IsoNumber": 7500,
                "IsoActivityHistory": "history",
                "IsoMerchantActivity": "merchants",
                "IsoName": "1st Merchant Services LLC",
                "ActiveofMerchants": 0,
                "NewofMerchantsMTD": 0,
                "ClosedofMerchantsMTD": 0,
                "ofTransLastMTD": 0,
                "ofTransThisMTD": 0,
                "TransVariancepercentage": 0,
                "SumAmountLastMTD": 0,
                "SumAmountThisMTD": 0,
                "SumAmountVariancepercentage": 0,
                "ofTransLastMonth": 0,
                "SumAmountLastMonth": 0,
                "ofTransYTD": 0,
                "SumAmountYTD": 0
            },
            {
                "IsoNumber": 7900,
                "IsoActivityHistory": "history",
                "IsoMerchantActivity": "merchants",
                "IsoName": "610 Merchant Services LLC",
                "ActiveofMerchants": 0,
                "NewofMerchantsMTD": 0,
                "ClosedofMerchantsMTD": 0,
                "ofTransLastMTD": 0,
                "ofTransThisMTD": 0,
                "TransVariancepercentage": 0,
                "SumAmountLastMTD": 0,
                "SumAmountThisMTD": 0,
                "SumAmountVariancepercentage": 0,
                "ofTransLastMonth": 0,
                "SumAmountLastMonth": 0,
                "ofTransYTD": 0,
                "SumAmountYTD": 0
            },
            {
                "IsoNumber": 35500,
                "IsoActivityHistory": "history",
                "IsoMerchantActivity": "merchants",
                "IsoName": "AAA Viza",
                "ActiveofMerchants": 1,
                "NewofMerchantsMTD": 0,
                "ClosedofMerchantsMTD": 0,
                "ofTransLastMTD": 6,
                "ofTransThisMTD": 1,
                "TransVariancepercentage": -83.33,
                "SumAmountLastMTD": 2056,
                "SumAmountThisMTD": 49,
                "SumAmountVariancepercentage": -97.62,
                "ofTransLastMonth": 11,
                "SumAmountLastMonth": 3649,
                "ofTransYTD": 1,
                "SumAmountYTD": 49
            },
            {
                "IsoNumber": 12500,
                "IsoActivityHistory": "history",
                "IsoMerchantActivity": "merchants",
                "IsoName": "Advanced Payments Systems LLC",
                "ActiveofMerchants": 2,
                "NewofMerchantsMTD": 0,
                "ClosedofMerchantsMTD": 0,
                "ofTransLastMTD": 6,
                "ofTransThisMTD": 5,
                "TransVariancepercentage": -16.67,
                "SumAmountLastMTD": 720,
                "SumAmountThisMTD": 420,
                "SumAmountVariancepercentage": -41.67,
                "ofTransLastMonth": 10,
                "SumAmountLastMonth": 1150,
                "ofTransYTD": 5,
                "SumAmountYTD": 420
            },
            {
                "IsoNumber": 12500,
                "IsoActivityHistory": "history",
                "IsoMerchantActivity": "merchants",
                "IsoName": "BCSW Schools Direct",
                "ActiveofMerchants": 724,
                "NewofMerchantsMTD": 1,
                "ClosedofMerchantsMTD": 0,
                "ofTransLastMTD": 102314,
                "ofTransThisMTD": 87298,
                "TransVariancepercentage": -14.67,
                "SumAmountLastMTD": 4476457,
                "SumAmountThisMTD": 4379856,
                "SumAmountVariancepercentage": -2.67,
                "ofTransLastMonth": 173652,
                "SumAmountLastMonth": 7804817,
                "ofTransYTD": 87298,
                "SumAmountYTD": 4379856
            },
            {
                "IsoNumber": 1800,
                "IsoActivityHistory": "history",
                "IsoMerchantActivity": "merchants",
                "IsoName": "Affinity Merchants Solutions",
                "ActiveofMerchants": 0,
                "NewofMerchantsMTD": 0,
                "ClosedofMerchantsMTD": 0,
                "ofTransLastMTD": 0,
                "ofTransThisMTD": 0,
                "TransVariancepercentage": 0,
                "SumAmountLastMTD": 0,
                "SumAmountThisMTD": 0,
                "SumAmountVariancepercentage": 0,
                "ofTransLastMonth": 0,
                "SumAmountLastMonth": 0,
                "ofTransYTD": 0,
                "SumAmountYTD": 0
            },
            {
                "IsoNumber": 5650,
                "IsoActivityHistory": "history",
                "IsoMerchantActivity": "merchants",
                "IsoName": "Agape Global Services",
                "ActiveofMerchants": 0,
                "NewofMerchantsMTD": 0,
                "ClosedofMerchantsMTD": 0,
                "ofTransLastMTD": 0,
                "ofTransThisMTD": 0,
                "TransVariancepercentage": 0,
                "SumAmountLastMTD": 0,
                "SumAmountThisMTD": 0,
                "SumAmountVariancepercentage": 0,
                "ofTransLastMonth": 0,
                "SumAmountLastMonth": 0,
                "ofTransYTD": 0,
                "SumAmountYTD": 0
            },
            {
                "IsoNumber": 12500,
                "IsoActivityHistory": "history",
                "IsoMerchantActivity": "merchants",
                "IsoName": "AMG Payments Solutions",
                "ActiveofMerchants": 13,
                "NewofMerchantsMTD": 0,
                "ClosedofMerchantsMTD": 0,
                "ofTransLastMTD": 658,
                "ofTransThisMTD": 707,
                "TransVariancepercentage": 7.67,
                "SumAmountLastMTD": 4476457,
                "SumAmountThisMTD": 4379856,
                "SumAmountVariancepercentage": 2.67,
                "ofTransLastMonth": 173652,
                "SumAmountLastMonth": 7804817,
                "ofTransYTD": 87298,
                "SumAmountYTD": 4379856
            }

        ];



        var report2 = [
            {
                "Months": "2015-06",
                "NewofMerchantsMTD": "6",
                "ClosedofMerchantsMTD": "6",
                "ofTransMTD": "37600",
                "ofTransMTDVariancepercentage": "-5.11",
                "SumAmountThisMTD": "14205911",
                "SumAmountMTDVariance": "-9.56",
                "ofTransMonth": "113908",
                "ofTransMonthVariancepercentage": "89.71",
                "SumAmountMonth": "29609280",
                "SumAmountMonthVariancepercentage": "3.52"
            },
            {
                "Months": "2015-06",
                "NewofMerchantsMTD": "2",
                "ClosedofMerchantsMTD": "3",
                "ofTransMTD": "37513",
                "ofTransMTDVariancepercentage": "-1.194",
                "SumAmountThisMTD": "15299414",
                "SumAmountMTDVariance": "7.70",
                "ofTransMonth": "55899",
                "ofTransMonthVariancepercentage": "-50.123",
                "SumAmountMonth": "30951512",
                "SumAmountMonthVariancepercentage": "4.43"
            },
            {
                "Months": "2015-06",
                "NewofMerchantsMTD": "2",
                "ClosedofMerchantsMTD": "2",
                "ofTransMTD": "3805",
                "ofTransMTDVariancepercentage": "12.18",
                "SumAmountThisMTD": "17926667",
                "SumAmountMTDVariance": "9.56",
                "ofTransMonth": "63457",
                "ofTransMonthVariancepercentage": "11.67",
                "SumAmountMonth": "34268314",
                "SumAmountMonthVariancepercentage": "6.83"
            },
            {
                "Months": "2015-06",
                "NewofMerchantsMTD": 0,
                "ClosedofMerchantsMTD": "1",
                "ofTransMTD": "8411",
                "ofTransMTDVariancepercentage": "6",
                "SumAmountThisMTD": "46088551",
                "SumAmountMTDVariance": "9.56",
                "ofTransMonth": "9952",
                "ofTransMonthVariancepercentage": "8.68",
                "SumAmountMonth": "57096585",
                "SumAmountMonthVariancepercentage": "13.13"
            },
            {
                "Months": "2015-06",
                "NewofMerchantsMTD": "2",
                "ClosedofMerchantsMTD": "2",
                "ofTransMTD": "45974",
                "ofTransMTDVariancepercentage": "12.90",
                "SumAmountThisMTD": "24846745",
                "SumAmountMTDVariance": "22.51",
                "ofTransMonth": "45974",
                "ofTransMonthVariancepercentage": "-45.68",
                "SumAmountMonth": "24846465",
                "SumAmountMonthVariancepercentage": "-50.10"
            },
            {
                "Months": "2015-06",
                "NewofMerchantsMTD": "6",
                "ClosedofMerchantsMTD": "6",
                "ofTransMTD": "37600",
                "ofTransMTDVariancepercentage": "-5.11",
                "SumAmountThisMTD": "14205911",
                "SumAmountMTDVariance": "-9.56",
                "ofTransMonth": "113908",
                "ofTransMonthVariancepercentage": "89.71",
                "SumAmountMonth": "29609280",
                "SumAmountMonthVariancepercentage": "3.52"
            },
            {
                "Months": "2015-06",
                "NewofMerchantsMTD": "2",
                "ClosedofMerchantsMTD": "3",
                "ofTransMTD": "37513",
                "ofTransMTDVariancepercentage": "-1.194",
                "SumAmountThisMTD": "15299414",
                "SumAmountMTDVariance": "7.70",
                "ofTransMonth": "55899",
                "ofTransMonthVariancepercentage": "-50.123",
                "SumAmountMonth": "30951512",
                "SumAmountMonthVariancepercentage": "4.43"
            },
            {
                "Months": "2015-06",
                "NewofMerchantsMTD": "2",
                "ClosedofMerchantsMTD": "2",
                "ofTransMTD": "3805",
                "ofTransMTDVariancepercentage": "12.18",
                "SumAmountThisMTD": "17926667",
                "SumAmountMTDVariance": "9.56",
                "ofTransMonth": "63457",
                "ofTransMonthVariancepercentage": "11.67",
                "SumAmountMonth": "34268314",
                "SumAmountMonthVariancepercentage": "6.83"
            },
            {
                "Months": "2015-06",
                "NewofMerchantsMTD": 0,
                "ClosedofMerchantsMTD": "1",
                "ofTransMTD": "8411",
                "ofTransMTDVariancepercentage": "6",
                "SumAmountThisMTD": "46088551",
                "SumAmountMTDVariance": "9.56",
                "ofTransMonth": "9952",
                "ofTransMonthVariancepercentage": "8.68",
                "SumAmountMonth": "57096585",
                "SumAmountMonthVariancepercentage": "13.13"
            },
            {
                "Months": "2015-06",
                "NewofMerchantsMTD": "2",
                "ClosedofMerchantsMTD": "2",
                "ofTransMTD": "45974",
                "ofTransMTDVariancepercentage": "12.90",
                "SumAmountThisMTD": "24846745",
                "SumAmountMTDVariance": "22.51",
                "ofTransMonth": "45974",
                "ofTransMonthVariancepercentage": "-45.68",
                "SumAmountMonth": "24846465",
                "SumAmountMonthVariancepercentage": "-50.10"
            },
        ];
        var report3 = [
            {
                "MerchantID": "77005",
                "MerchantName": "Austin T3",
                "ofTransLastMTD": "6",
                "ofTransThisMTD": "5",
                "ofTransVariance": "-16.06",
                "SumAmountLastMTD": "720",
                "SumAmountThisMTD": "420",
                "SumAmountVariancepercentage": "-41.67",
                "ofTransLastMonth": "10",
                "SumAmountLastMonth": "1150",
                "ofTransYTD": "5",
                "SumAmountYTD": "420"
            },
            {
                "MerchantID": "78001",
                "MerchantName": "Foremost Carpets & Interors INC",
                "ofTransLastMTD": 0,
                "ofTransThisMTD": 0,
                "ofTransVariance": 0,
                "SumAmountLastMTD": 0,
                "SumAmountThisMTD": 0,
                "SumAmountVariancepercentage": 0,
                "ofTransLastMonth": 0,
                "SumAmountLastMonth": 0,
                "ofTransYTD": 0,
                "SumAmountYTD": 0
            },
            {
                "MerchantID": "77005",
                "MerchantName": "Austin T3",
                "ofTransLastMTD": "6",
                "ofTransThisMTD": "5",
                "ofTransVariance": "-16.06",
                "SumAmountLastMTD": "720",
                "SumAmountThisMTD": "420",
                "SumAmountVariancepercentage": "-41.67",
                "ofTransLastMonth": "10",
                "SumAmountLastMonth": "1150",
                "ofTransYTD": "5",
                "SumAmountYTD": "420"
            },
            {
                "MerchantID": "78001",
                "MerchantName": "Foremost Carpets & Interors INC",
                "ofTransLastMTD": 0,
                "ofTransThisMTD": 0,
                "ofTransVariance": 0,
                "SumAmountLastMTD": 0,
                "SumAmountThisMTD": 0,
                "SumAmountVariancepercentage": 0,
                "ofTransLastMonth": 0,
                "SumAmountLastMonth": 0,
                "ofTransYTD": 0,
                "SumAmountYTD": 0
            },
            {
                "MerchantID": "77005",
                "MerchantName": "Austin T3",
                "ofTransLastMTD": "6",
                "ofTransThisMTD": "5",
                "ofTransVariance": "-16.06",
                "SumAmountLastMTD": "720",
                "SumAmountThisMTD": "420",
                "SumAmountVariancepercentage": "-41.67",
                "ofTransLastMonth": "10",
                "SumAmountLastMonth": "1150",
                "ofTransYTD": "5",
                "SumAmountYTD": "420"
            },
            {
                "MerchantID": "78001",
                "MerchantName": "Foremost Carpets & Interors INC",
                "ofTransLastMTD": 0,
                "ofTransThisMTD": 0,
                "ofTransVariance": 0,
                "SumAmountLastMTD": 0,
                "SumAmountThisMTD": 0,
                "SumAmountVariancepercentage": 0,
                "ofTransLastMonth": 0,
                "SumAmountLastMonth": 0,
                "ofTransYTD": 0,
                "SumAmountYTD": 0
            }



        ];



        return {
            report1: function () {

                return report1;
            },
            report2: function () {
                return report2;
            },
            report3: function () {
                return report3;
            }
        }
    }

    ReportFactory.$inject = ['$http', '$window', 'settings'];

    angular.module('evericheck').factory('ReportFactory', ReportFactory);

})();