/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('evericheck')
        .factory('MerchantActivityFactory', function MerchantActivityFactory($q, $http, settings) {
            var exports = {};
            exports.merchantActivityReports = [];

            exports.getMerchantMonthlyActivity = function (extraParam,mid) {
                var deferred = $q.defer();
                return $http({
                    method: 'GET',
                    url: settings.baseUrl + '/merchant-monthly-activity/'+mid,
                    params: extraParam
                }).success(function (data) {
                    exports.merchantActivityReports = data;
                    deferred.resolve(data);

                })
                        .error(function (data) {
                            deferred.reject(data);
                        });
                return deferred.promise;

            };
           
            return exports;


        });

