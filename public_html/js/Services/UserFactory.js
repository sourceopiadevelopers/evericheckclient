(function () {

    function UserFactory($http, settings, $window) {
        var users = {};
        return {
            set: function (data) {
                $window.localStorage.setItem('user', JSON.stringify(data));
            },
            
            get: function () {
                if ($window.localStorage.getItem('user') !== null)
                {
                    return JSON.parse($window.localStorage.getItem('user'));
                }

            },
            
            add: function (user, successCallback, errorCallback) {
                $http({
                    method: 'POST',
                    url: settings.baseUrl + '/users',
                    data: user,
                    headers: {'Authorization': $window.localStorage.getItem('Authorization')}
                })
                .success(function (data) {
                    successCallback(data);
                })
                .error(function (data) {
                    errorCallback(data);
                });
            },
            
            edit: function (user, successCallback, errorCallback) {
                $http({
                    method: 'PUT',
                    url: settings.baseUrl + '/users', 
                    params: user,
                    headers: {'Authorization': $window.localStorage.getItem('Authorization')}
                })
                .success(function (data) {
                    successCallback(data);
                })
                .error(function (data) {
                    errorCallback(data);
                });
            },
            
            view: function (successCallback, errorCallback) {
                    $http({
                        method: 'GET',
                        url: settings.baseUrl + '/users',
                        useXDomain: true,
                        headers: {'Authorization': $window.localStorage.getItem('Authorization')}
                    })
                    .success(function (data) {
                        users = data;
                        $window.localStorage.setItem('totalUserItems',(data.payload).length);
                        successCallback(data);
                    })
                    .error(function (data) {
                        errorCallback(data);
                    });
            },
            
            changeUserPassword: function (user, successCallback, errorCallback) {
                $http({
                    method: 'PUT',
                    url: settings.baseUrl + '/users',
                    params: user,
                    headers: {'Authorization': $window.localStorage.getItem('Authorization')}
                })
                .success(function (data) {
                    successCallback(data);
                })
                .error(function (data) {
                    errorCallback(data);
                });
            },

        };
    }

    UserFactory.$inject = ['$http', 'settings', '$window'];

    angular.module('evericheck').factory('UserFactory', UserFactory);

})();