(function () {


    function LoginFactory($http, $window, settings) {
        return {
            login: function (data, successCallback, errorCallback) {
                $http.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded;charset=utf-8';
                delete $http.defaults.headers.common['Authorization'];
                settings.username = data.name;
                settings.password = data.password;
                $http({
                    method: 'POST',
                    url: settings.baseUrl + '/login',
                    data: data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                })
                .success(function (data) {
                    $http.defaults.headers.common['Authorization'] = 'Bearer ' + data.token;
                    successCallback(data);
                })
                .error(function (data) {
                    errorCallback(data);
                });
            }
        };
    }

    LoginFactory.$inject = ['$http', '$window', 'settings'];

    angular.module('evericheck').factory('LoginFactory', LoginFactory);

})();