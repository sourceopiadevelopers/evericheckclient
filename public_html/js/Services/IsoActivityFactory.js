/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('evericheck')
        .factory('IsoActivityFactory', function IsoActivityFactory($q, $http, settings) {
            var exports = {};
            exports.isoActivityReports = [];
            exports.isosCurrentMonthActivityReports=[];

            exports.getIsoMonthlyActivity = function (extraParam,isoId) {
                var deferred = $q.defer();
                return $http({
                    method: 'GET',
                    url: settings.baseUrl + '/iso-monthly-activity/'+isoId,
                    params: extraParam
                }).success(function (data) {
                    exports.isoActivityReports = data;
                    deferred.resolve(data);

                })
                        .error(function (data) {
                            deferred.reject(data);
                        });
                return deferred.promise;

            };
            
            exports.getIsoMerchantsMonthlyActivity = function (isoId) {
              var deferred = $q.defer();
                return $http({
                    method: 'GET',
                    url: settings.baseUrl + '/iso-merchants-currentmonth/activity/'+isoId
                }).success(function (data) {
                    exports.isoMerchantActivityReports = data;
                    deferred.resolve(data);
                })
                        .error(function (data) {
                            deferred.reject(data);
                        });
                return deferred.promise;  
            };
            
            exports.getIsosCurrentMonthActivity = function () {
                var deferred = $q.defer();
                return $http({
                    method: 'GET',
                    url: settings.baseUrl + '/isos-currentmonth-activity',
                    
                }).success(function (data) {
                    exports.isosCurrentMonthActivityReports = data;
                    deferred.resolve(data);

                })
                        .error(function (data) {
                            deferred.reject(data);
                        });
                return deferred.promise;

            };
           
            return exports;
        });

