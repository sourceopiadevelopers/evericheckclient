(function(){

    function MerchantFactory($http, $window, settings) {
        return {
            fetch: function (successCallback, errorCallback) {
                    $http({
                        method: 'GET',
                        url: settings.baseUrl + '/merchants',
                        useXDomain: true,
                        headers: {'Authorization': $window.localStorage.getItem('Authorization')}
                    })
                        .success(function (data) {
                            successCallback(data);
                        })
                        .error(function (data) {
                            errorCallback(data);
                        });
            },
            fetchById: function (merchantId, successCallback, errorCallback) {
                    $http({
                        method: 'GET',
                        url: settings.baseUrl + '/merchants',
                        useXDomain: true,
                        params: merchantId,
                        headers: {'Authorization': $window.localStorage.getItem('Authorization')}
                    })
                        .success(function (data) {
                            successCallback(data);
                        })
                        .error(function (data) {
                            errorCallback(data);
                        });
            },
            fetchByIsoId: function (isoId, successCallback, errorCallback) {
                    $http({
                        method: 'GET',
                        url: settings.baseUrl + '/merchants',
                        useXDomain: true,
                        params: isoId,
                        headers: {'Authorization': $window.localStorage.getItem('Authorization')}
                    })
                        .success(function (data) {
                            successCallback(data);
                        })
                        .error(function (data) {
                            errorCallback(data);
                        });
            },
            fetchMerchantCurrentMonthActivity: function (extraParam,successCallback, errorCallback) {
                    $http({
                        method: 'GET',
                        url: settings.baseUrl + '/merchants-currentmonth-activity',
                        params: extraParam,
                        useXDomain: true
                    })
                        .success(function (data) {
                            successCallback(data);
                        })
                        .error(function (data) {
                            errorCallback(data);
                        });
            }
        };
    }

    MerchantFactory.$inject = ['$http', '$window', 'settings'];

    angular.module('evericheck').factory('MerchantFactory', MerchantFactory);

})();