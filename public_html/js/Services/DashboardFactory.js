(function(){
    
    
    function DashboardFactory($http, settings, $window) {
        return {
            dashboard: function(){
                    $http({
                            method: 'GET',
                            url: settings.baseUrl + '/dashboard',
                            useXDomain:true,
                            headers: {'Authorization': $window.localStorage.getItem('Authorization')}
                    })
                    .success(function (data) {
                            console.log(data);
                    })
                    .error(function (data) {
                            console.log('ERROR');
                    });
            }
        };
    }
    
    DashboardFactory.$inject = ['$http','settings','$window'];
    
    angular.module('evericheck').factory('DashboardFactory', DashboardFactory);
    
})();