/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
angular.module('evericheck')
    .factory('BatchFactory', function BatchFactory($q, $http, settings) {
        var exports = {};
        exports.batches = [];

        exports.getBatches = function (extraParam, id) {
            var deferred = $q.defer();
            return $http({
                method: 'GET',
                url: settings.baseUrl + '/batches/' + id,
                params: extraParam
            }).success(function (data) {
                    exports.batches = data;
                    deferred.resolve(data);

                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;

        };
        
        exports.getBatchesByDate = function (extraParam) {
            var deferred = $q.defer();
            return $http({
                method: 'GET',
                url: settings.baseUrl + '/batches',
                params: extraParam
            }).success(function (data) {
                    exports.batches = data;
                    deferred.resolve(data);

                })
                .error(function (data) {
                    deferred.reject(data);
                });
            return deferred.promise;

        };
        return exports;


    });

