(function(){
    
    "use strict";
    
    function DashboardController($scope, DashboardFactory) {
        
        $scope.$on('$viewContentLoaded', function() {   
            App.initAjax(); // initialize core components   
            //DashboardFactory.dashboard();
        });
    }
    
    DashboardController.$inject = ['$scope','DashboardFactory'];
    
    angular.module('evericheck').controller('DashboardController', DashboardController);
    
})()