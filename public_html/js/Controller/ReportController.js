(function () {

    "use strict";

    function ReportController($scope, ReportFactory, BatchFactory, MerchantActivityFactory, IsoActivityFactory, $stateParams, $filter,$timeout) {
        $scope.show = {
            repeatSelect: null,
            availableOptions: [
                {id: '5', name: '5'},
                {id: '50', name: '50'},
                {id: '100', name: '100'},
                {id: null, name: 'All'}
            ]
        };

        $scope.orderByField = '';
        $scope.reverseSort = false;

        $scope.currentPage = 1;
        $scope.repeatSelect = $scope.show.availableOptions[0].id;
        $scope.pageSize = $scope.repeatSelect;


        $scope.report1 = ReportFactory.report1();
        $scope.report2 = ReportFactory.report2();
        $scope.report3 = ReportFactory.report3();
        $scope.loadBathchesByDate = false;
        $scope.loadBathchesByIso = false;
        $scope.loadBathchesByMerchant = false;
        $scope.loadMerchantActivity = false;
        $scope.loadIsoActivity = false;
        $scope.loadIsoMerchantsActivity = false;
        $scope.loadIsosCurrentMonthActivity = false;



        /*
         * parse the amount from string to number
         */
        $scope.parseIntoInteger = function (val) {
            val.IsoNumber = parseInt(val.IsoNumber);
        };

        /*
         * fetches batches using date
         */
        $scope.date = (new moment().subtract(29, 'days').format("YYYY-MM-DD"))+" - "+(new moment().format("YYYY-MM-DD"));
        $scope.fetchBatchesByDate = function () {
            $scope.loading = true;
            $scope.responseBatchesByDate = [];
            $scope.batchParams = {};

            if($scope.date === '' && moment($scope.date, 'YYYY-MM-DD', false)) {
                $scope.loading = false;
                $scope.loadBathchesByDate = false;
            } else {
                var dates = $scope.date.split(" ");
                $scope.batchParams.start_date = dates[0].trim();
                $scope.batchParams.end_date = dates[2].trim();

                BatchFactory.getBatchesByDate($scope.batchParams)
                    .then(angular.bind(this, function then() {
                        $scope.responseBatchesByDate = BatchFactory.batches;
                        $scope.loadBathchesByDate = true;
                        $scope.loading = false;
                    }));
            }

        };
        /*
         * fetches batches using batch id
         */
        $scope.fetchBatchesById = function (id) {
            $scope.responseBatchesById = [];
            $scope.batchParams = {};
            BatchFactory.getBatches($scope.batchParams,id)
                    .then(angular.bind(this, function then() {
                        $scope.responseBatchesById = BatchFactory.batches;
                        $scope.loadBathchesById = true;
                    }));

        };

        /*
         * fetches batches using batch id and iso id
         */
        $scope.fetchBatchesByIso = function (id, iso_id) {
            $scope.loadBathchesByIso = false;
            $scope.responseBatchesByIso = [];
            $scope.batchParams = {};
            $scope.batchParams.iso = iso_id;
            $scope.isoID = iso_id;
            BatchFactory.getBatches($scope.batchParams,id)
                    .then(angular.bind(this, function then() {
                        $scope.responseBatchesByIso = BatchFactory.batches;
                        $scope.loadBathchesByIso = true;
                    }));
        };

        /*
         * fetches batches using batch id and merchant id
         */
        $scope.fetchBatchesByMerchant = function (id, merchant_id) {
            $scope.loadBathchesByMerchant = false;
            $scope.responseBatchesByMerchant = [];
            $scope.batchParams = {};
            $scope.batchParams.merchant = merchant_id;
            $scope.merchantId = merchant_id;
            BatchFactory.getBatches($scope.batchParams,id)
                    .then(angular.bind(this, function then() {
                        $scope.responseBatchesByMerchant = BatchFactory.batches;
                        $scope.loadBathchesByMerchant = true;
                    }));
        };

        /**
         * MerchantId validation for Monthly Merchant Activity. That takes any integer except zero.
         *
         * @param merchantID int merchantID to find Merchant Monthly Activity.
         */
        $scope.MerchantIDValidator = function (merchantId) {
            if (!merchantId) {
                return;
            }
            if (!merchantId.match(/^[1-9]\d*$/)) {
                return "Only digits. Zeros not allowed";
            }
            return true;
        };

        /*
         * fetches monthly merchant activity using date range and merchant id
         */
        $scope.fetchMonthlyMerchantActivity = function () {
            $scope.loading = true;
            $scope.merchantActivity = [];
            $scope.merchant_ids = ['111','1123','23434'];
            $scope.monthlyMerchantParams = {};
            $scope.total = {};
            

            if($scope.date === '' && moment($scope.date, 'YYYY-MM-DD', false)) {
                $scope.loading = false;
                $scope.loadMerchantActivity = false;
            } else {
                var dates = $scope.date.split(" ");
                $scope.monthlyMerchantParams.start_date = dates[0].trim();
                $scope.monthlyMerchantParams.end_date = dates[2].trim();
                MerchantActivityFactory.getMerchantMonthlyActivity($scope.monthlyMerchantParams, $scope.merchant_id)
                    .then(angular.bind(this, function then() {
                        $scope.merchantActivity = MerchantActivityFactory.merchantActivityReports;
                        $scope.merchantActivity = $scope.merchantActivity.payload;
                        $scope.loadMerchantActivity = true;
                        $scope.loading = false;
                        

                    }));
            }

        };

        /**
         * IsoID validation for Iso Monthly Activity. That takes any integer except zero.
         *
         * @param isoId int isoId To find Iso Monthly Activity
         */
        $scope.IsoIDValidator = function (isoId) {
            if (!isoId) {
                return;
            }
            if (!isoId.match(/^[1-9]\d*$/)) {
                return "Only digits. Zeros not allowed";
            }
            return true;
        };

        /**
         * fetches monthly iso activity using date range and iso id
         **/
        $scope.fetchMonthlyIsoActivity = function () {
            $scope.loading = true;
            $scope.isoActivity = [];
            $scope.monthlyIsoParams = {};
            if($scope.date === '' && moment($scope.date, 'YYYY-MM-DD', false)) {
                $scope.loading = false;
                $scope.loadIsoActivity = false;
            } else {
                var dates = $scope.date.split(" ");
                $scope.monthlyIsoParams.start_date = dates[0].trim();
                $scope.monthlyIsoParams.end_date = dates[2].trim();
                IsoActivityFactory.getIsoMonthlyActivity($scope.monthlyIsoParams,$scope.iso_id)
                    .then(angular.bind(this, function then() {
                        $scope.isoActivity = IsoActivityFactory.isoActivityReports;
                        $scope.isoActivity = $scope.isoActivity.payload;
                        $scope.loadIsoActivity = true;
                        $scope.loading = false;
                    }));
            }

        };

        /* fetches iso's merchants current month activity using iso id
         */
        $scope.fetchIsoMerchantsCurrentMonthActivity = function () {
            $scope.loading = true;
            $scope.isoMerchantsActivity = [];
            IsoActivityFactory.getIsoMerchantsMonthlyActivity($scope.iso_id)
                    .then(angular.bind(this, function then() {
                        $scope.isoMerchantsActivity = IsoActivityFactory.isoMerchantActivityReports;
                        $scope.isoMerchantsActivity = $scope.isoMerchantsActivity.payload;
                        $scope.loadIsoMerchantsActivity = true;
                        $scope.loading = false;
                    }));
        };



        /*
         * fetches isos current month activity
         */
        $scope.fetchIsosCurrentMonthActivity = function () {
            $scope.loading = true;
            IsoActivityFactory.getIsosCurrentMonthActivity()
                    .then(angular.bind(this, function then() {
                        $scope.isosCurrentMonthActivityData = IsoActivityFactory.isosCurrentMonthActivityReports.payload;
                        $scope.loadIsosCurrentMonthActivity = true;
                        $scope.loading = false;
                    }));
        };



//         $scope.isoMonthlyActivityTotal =
//             {
//                 'total_new_mtd_merchants_count':0,
//                 'total_closed_mtd_merchants_count':0,
//                 'total_txn_count_mtd':0,
//                 'total_mtd_txn_count_variance':0,
//                 'total_mtd_txn_sum':0,
//                 'total_mtd_txn_sum_variance':0,
//                 'total_txn_count':0,
//                 'total_txn_count_variance':0,
//                 'total_txn_sum':0,
//                 'total_txn_sum_variance':0
//            };
//
//
//        $scope.countTotalIsoMonthlyActivity = function (data){
//             if(data){
//               if(typeof data.mtd_merchants_new_count !== 'undefined'){
//               $scope.isoMonthlyActivityTotal['total_new_mtd_merchants_count'] += data.mtd_merchants_new_count;
//               $scope.isoMonthlyActivityTotal['total_closed_mtd_merchants_count'] += data.mtd_merchants_closed_count;
//               $scope.isoMonthlyActivityTotal['total_txn_count_mtd'] += data.mtd_txn_count;
//               $scope.isoMonthlyActivityTotal['total_mtd_txn_count_variance'] += data.mtd_txn_count_variance;
//               $scope.isoMonthlyActivityTotal['total_mtd_txn_sum'] += data.mtd_txn_sum;
//               $scope.isoMonthlyActivityTotal['total_mtd_txn_sum_variance'] += data.mtd_txn_sum_variance;
//               $scope.isoMonthlyActivityTotal['total_txn_count'] += data.txn_count;
//               $scope.isoMonthlyActivityTotal['total_txn_count_variance'] += data.txn_count_variance;
//               $scope.isoMonthlyActivityTotal['total_txn_sum'] += data.txn_sum;
//               $scope.isoMonthlyActivityTotal['total_txn_sum_variance'] += data.txn_sum_variance;
//                }
//           }
//        };
//

        $scope.parseNumber = function (data, key) {
            data[key] = parseInt(data[key]);
            $scope.testData = data[key];
        };

    }

    ReportController.$inject = ['$scope', 'ReportFactory', 'BatchFactory', 'MerchantActivityFactory', 'IsoActivityFactory', '$stateParams', '$filter','$timeout'];
    angular.module('evericheck').controller('ReportController', ReportController);

})();