(function () {
    "use strict";
    function MerchantController($scope, MerchantFactory) {
            $scope.loading = true;
//            $scope.$on('$viewContentLoaded', function () {
//                App.initAjax(); // initialize core components
//                MerchantFactory.fetch(
//                        function (data) {
//                            $scope.merchantsData = data.payload;
//                        },
//                        function (data) {
//                            console.log(data);
//                        }
//                );
//            });
            $scope.merchant = {};
            $scope.loadMerchantCurrentMonthActivity = false;
//            $scope.findMerchantById = function (merchantId) {
//              $scope.merchant.id = merchantId.id;
//                MerchantFactory.fetchById($scope.merchant, 
//                    function (data) {
//                        if(data.success === true) {
//                            $scope.merchantByIdData = data.payload;
//                            $scope.merchantById = true;
//                        }
//                    },
//                function(error) {
//                    console.log(error);
//                });  
//            };
            
              $scope.parseNumber = function (data,key) {
            data[key] = parseInt(data[key]);
            return data[key];
        };
            
            
            $scope.merchantIso = {};
//            $scope.findMerchantByIsoId = function (isoId) {
//                $scope.merchantIso.iso = isoId.iso;
//                    MerchantFactory.fetchByIsoId($scope.merchantIso, 
//                    function (data) {
//                        if(data.success === true) {
//                            $scope.merchantsByIsoData = data.payload;
//                            $scope.merchantByIso = true;
//                        }
//                    },
//                function(error) {
//                    console.log(error);
//                });
//                
//            };
//            
//            
//            $scope.$on('$viewContentLoaded', function () {
//                $scope.loading = true;
//                App.initAjax(); // initialize core components
//                MerchantFactory.fetchMerchantCurrentMonthActivity(
//                        $scope.currentMonthlyMerchantParams,
//                        function (data) {
//                            $scope.merchantsCurrentMonthActivityData = data.payload;
//                            $scope.loadMerchantCurrentMonthActivity = true;
//                            $scope.loading = false;
//                        },
//                        function (data) {
//                            console.log(data);
//                        }
//                );
//            });




        $scope.totalItems = 64;
        $scope.currentPage = 1;
        
        $scope.setPage = function (pageNo)
        {
            $scope.loadMerchantCurrentMonthActivity = false;
            $scope.loading = true;
            $scope.currentPage = pageNo;
            $scope.total = {};
            $scope.currentMonthlyMerchantParams={};
            $scope.currentMonthlyMerchantParams.page = $scope.currentPage;
            MerchantFactory.fetchMerchantCurrentMonthActivity(
                        $scope.currentMonthlyMerchantParams,
                        function (data) {
                            $scope.merchantsCurrentMonthActivityData = data.payload;
                            $scope.loadMerchantCurrentMonthActivity = true;
                            $scope.loading = false;
                        },
                        function (data) {
                            console.log(data);
                        }
                );
            
        };
        $scope.pageChanged = function ()
        {
//            console.log('Page changed to: ' + $scope.currentPage);
            console.log('Page changed to: ' + $scope.bigCurrentPage);
            $scope.setPage($scope.bigCurrentPage);
            

        };
        $scope.maxSize = 5;
        $scope.bigTotalItems = 175;
        $scope.bigCurrentPage = 1;
        };
    
    MerchantController.$inject = ['$scope', 'MerchantFactory'];
    angular.module('evericheck').controller('MerchantController', MerchantController);

})();

