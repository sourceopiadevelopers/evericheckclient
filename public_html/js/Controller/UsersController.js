(function () {

    "use strict";

    function UsersController($scope, $state, settings, flash, UserFactory, MerchantFactory) {

        settings.layout.pageContentWhite = true;
        settings.layout.pageBodySolid = true;
        settings.layout.pageSidebarClosed = true;
        
        $scope.changeUser = {};
        $scope.createUser = {};
        $scope.editUser = {};
        $scope.usersPattern = {password: '((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%]).{8,20})'};
        $scope.flash = flash;

        $scope.userNameLength = 15;
        $scope.userCreateNameLength = 15;
        $scope.userPasswordLength = 20;
        $scope.userCreatePasswordLength = 20;

        $scope.show = {
            repeatSelect: null,
            availableOptions: [
                {id: '20', name: '20'},
                {id: '50', name: '50'},
                {id: '100', name: '100'},
                {id: null, name: 'All'}

            ]
        };
        $scope.orderByField = 'requester';
        $scope.reverseSort = false;

        $scope.currentPage = 1;
        $scope.repeatSelect = $scope.show.availableOptions[0].id;
        $scope.pageSize = $scope.repeatSelect;

        $scope.$on('$viewContentLoaded', function () {
            App.initAjax();
            UserFactory.view(
                function (data) {
                    $scope.usersData = data.payload;
                    $scope.totalItems = (data.payload).length;
                },
                function (data) {
                    console.log(data);
                });
        });

        MerchantFactory.view(
            function (data) {
                $scope.merchants = data.payload;
            },
            function (data) {
                console.log(data);
            });

        $scope.create = function (createUsavailableOptionser) {
            var type = $('#typeSwitch').bootstrapSwitch('state');
            
            if (type) {
                $scope.createUser.type = 'Vericheck';
                var vericheckType = $('#vericheckTypeSwitch').bootstrapSwitch('state');
                if(vericheckType)
                {
                    $scope.createUser.parent_id = '1';
                } else 
                {
                    $scope.createUser.parent_id = '2';
                }
            } else {
                $scope.createUser.type = 'Merchant';
            }
            UserFactory.add($scope.createUser,
            function (data) {
                if (data.success === true)
                {
                    flash.setMessage(data.message);
                    $state.transitionTo("theme.users", {}, {inherit: false, reload:true, notify: true});

                } else
                {
                    flash.setMessage(data.message);
                }

            }, function (error) {
                console.log(error);
            });
        };

        //
        $scope.editClick = function (data) {
            UserFactory.set(data);
        };
        
        $scope.activeSwitch = function () {
            $scope.editUserForm.active.$setDirty();
        };

        $scope.editUser = UserFactory.get();
        if ($scope.editUser) {
            $scope.changeUser = $scope.editUser.User;
        }

        if ($scope.changeUser) {
            if ($scope.changeUser.active === 'no') {
                $('#activeSwitch').bootstrapSwitch('state', false);
            }
            else {
                $('#activeSwitch').bootstrapSwitch('state', true);
            }
        }


        $scope.$watch('changeUser.name', function (newValue, oldValue) {
            if (newValue) {
                if (newValue.length > 15) {
                    $scope. changeUser.name= oldValue;
                }
                $scope.userNameLength = 15 - newValue.length;
            }
        });


        $scope.$watch('createUser.name', function (newValue, oldValue) {
            if (newValue) {
                if (newValue.length > 15) {
                    $scope.createUser.name= oldValue;
                }
                $scope.userCreateNameLength = 15 - newValue.length;
            }
        });
        $scope.updateUserName = function (event) {
            return false;
        };


        $scope.$watch('changeUser.password', function (newValue, oldValue) {
            if (newValue) {
                if (newValue.length > 20) {
                    $scope.changeUser.password= oldValue;
                }
                $scope.userPasswordLength = 20 - newValue.length;
            }
        });

        $scope.$watch('createUser.pass', function (newValue, oldValue) {
            if (newValue) {
                if (newValue.length > 20) {
                    $scope.createUser.pass= oldValue;
                }
                $scope.userCreatePasswordLength = 20 - newValue.length;
            }
        });
        $scope.updateUserPassword = function (event) {
            return false;
        };


        $scope.edit = function (user) {
            
            $scope.changeUser = user;
            var active = $('#activeSwitch').bootstrapSwitch('state');
            
            if (active) {
                $scope.changeUser.active = 'yes';
            } else {
                $scope.changeUser.active = 'no';
            }
            
            UserFactory.edit(user,
            function (data) {
                if (data.success === true)
                {
                    flash.setMessage(data.message);
                    $state.transitionTo("theme.users", {}, {inherit: false, notify: true, reload:true});

                } else
                {
                    flash.setMessage(data.message);
                }

            }, function (error) {
                console.log(error);
            });
            
        };
        
        $scope.changePassword=function(user){
            $scope.changedPassword = {};
            $scope.changedPassword.newPassword = user.newPassword;
            UserFactory.changeUserPassword($scope.changedPassword,
            function(data){
                if (data.success === true)
                {
                    flash.setMessage(data.message);
                    $state.transitionTo("theme.users", {}, {inherit: false, reload: true, notify: true});
                } else
                {
                    flash.setMessage(data.message);
                    $state.transitionTo("theme.users/changePassword", {}, {inherit: false, reload: true, notify: true});
                }
            },function(error){
                console.log(error);
            });
        };

        $scope.cancel = function () {
            $state.transitionTo("theme.users", {}, {inherit: false, reload: true, notify: true});
        };
    }

    UsersController.$inject = ['$scope', '$state', 'settings', 'flash', 'UserFactory','MerchantFactory'];

    angular.module('evericheck').controller('UsersController', UsersController);

})();