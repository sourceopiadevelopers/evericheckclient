(function () {

    "use strict";

    function LoginController($scope, $timeout, $state, $window, LoginFactory) {
        $scope.login = function (user) {
            $window.localStorage.setItem('LoggedInUser', JSON.stringify(user));
            LoginFactory.login(user,
                    function (data) {
                        if (data.success === true)
                        {
                            $window.localStorage.setItem('loggedInId', data.id);
                            $window.localStorage.setItem('Authorization', 'Bearer ' + data.token);
                            $state.transitionTo("theme.dashboard", {}, {notify: true, inherit: false, reload:true});
                        } else if (data.success === true && data.type === 'Merchant') {
                            $window.localStorage.setItem('Authorization', 'Bearer ' + data.token);
                            $state.transitionTo("theme.dashboard", {}, {notify: true, inherit: false, reload:true});
                        }else{
                            $timeout(function () {
                                $scope.errorMessage = false;
                            }, 5000);
                            $scope.errorMessage = 'Invalid Authentication!! Please Try Again';
                            $window.localStorage.clear();
                        }

                    }, function (error) {
                $window.localStorage.clear();
                $scope.errorMessage = 'Invalid Authentication!! Please Try Again';
            });
        };

    }

    LoginController.$inject = ['$scope', '$timeout', '$state', '$window', 'LoginFactory'];

    angular.module('evericheck').controller('LoginController', LoginController);

})()