(function () {
    "use strict";
    function IsoController($scope, IsoFactory) {
        $scope.$on('$viewContentLoaded', function () {
            App.initAjax(); // initialize core components
            IsoFactory.fetch(
                    function (data) {
                        $scope.isosData = data.payload;
                    },
                    function (data) {
                        console.log(data);
                    }
            );
        });
        $scope.iso = {};
        $scope.findIsoById = function (isoId) {
            $scope.iso.id = isoId.id;
            IsoFactory.fetchById($scope.iso, 
                    function (data) {
                      if(data.success === true) {
                          $scope.isosByIdData = data.payload;
                          $scope.isosById = true;
                      }
                    },
                function(error) {
                    console.log(error);
                });        
            };
        };
    
    IsoController.$inject = ['$scope', 'IsoFactory'];
    angular.module('evericheck').controller('IsoController', IsoController);

})();
