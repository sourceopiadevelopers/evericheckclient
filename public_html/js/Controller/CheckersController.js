(function () {
    "use strict";
    function CheckersController($scope, BatchFactory, MerchantActivityFactory, IsoActivityFactory, $stateParams, $filter, $timeout) {
        $scope.date = (new moment().subtract(29, 'days').format("YYYY-MM-DD")) + " - " + (new moment().format("YYYY-MM-DD"));
        $scope.Math = window.Math;
        $scope.loadingCheckerMerchantMonthlyVsBatches = false;
        $scope.loadCheckersActivity = false;
        $scope.loadingCompletedOfbatchDetailVsAllMerchantsWithInABatch = false;
//        $scope.a = true;
//            $scope.b = true;
        $scope.loadIncompleteOfMonthlyMerchantActivity = true;
        $scope.loadIncompleteOfAllMerchantTotalBatchCount = true;
        $scope.loadInCompleteOfIsoMonthlyActivity = true;
        $scope.loadInCompleteOfAllIsoTotalBatchCount = true;
        

        /*
         * fetches batches using date 
         */
        $scope.date = (new moment().subtract(29, 'days').format("YYYY-MM-DD")) + " - " + (new moment().format("YYYY-MM-DD"));
        $scope.fetchAllBatchIdsOfDate = function () {
            $scope.loading = true;
            $scope.responseBatchesByDate = [];
            $scope.arrayOfId = [];
            $scope.batchParams = {};
            var dates = $scope.date.split(" ");
            $scope.batchParams.start_date = dates[0].trim();
            $scope.batchParams.end_date = dates[2].trim();
            BatchFactory.getBatchesByDate($scope.batchParams)
                .then(angular.bind(this, function then() {
                    $scope.responseBatchesByDate = BatchFactory.batches;
                    $scope.arrayOfId = [];

                    angular.forEach($scope.responseBatchesByDate['payload'], function (value, key) {
                        $scope.arrayOfId.push(value['id']);
                    });
                    $scope.loading = false;
                }));
        };
        $scope.fetchDataById = function () {
            $scope.loading=true;
            $scope.loadCheckersActivity = false;
            $scope.responseBatchesById = [];
            $scope.batchParams = {};
            $scope.createdCountTxnIdArray = [];
            $scope.creditArray = [];
            $scope.debitArray = [];
            $scope.reversalArray = [];
            $scope.loadCompleteOfSelectedArray = 0;
            angular.forEach($scope.selectedIdArray, function (value, key) {
                BatchFactory.getBatches($scope.batchParams, value)
                    .then(angular.bind(this, function then() {
                        $scope.responseBatchesById[key] = BatchFactory.batches;
                        $scope.calculateTransactionIdDifferenceAndCreatedCount(value, $scope.responseBatchesById[key].payload);
                        $scope.calculateCreditsDifference(value, $scope.responseBatchesById[key].payload.credits);
                        $scope.calculateDebitsDifference(value, $scope.responseBatchesById[key].payload.debits);
                        $scope.calculateReversalsDifference(value, $scope.responseBatchesById[key].payload.reversals);
                        
                        $scope.loadCompleteOfSelectedArray += 1;
                        if($scope.loadCompleteOfSelectedArray == $scope.selectedIdArray.length){
                            $scope.loadCheckersActivity = true;
                        $scope.loading = false;
                        }

                    }));
                    
            });

        };
        
        
        $scope.fetchData = function () {
            $scope.responseBatchesById = [];
            $scope.batchParams = {};
            $scope.allArrayInBatchVsMerchantFirstTest = {};
            $scope.allArrayInBatchVsMerchantSecondTest = {};
            $scope.allArrayInBatchVsMerchantThirdTest = {};
            $scope.allArrayInBatchVsMerchantFourthTest = {};
            
            
            
            $scope.selectedIdArrayLength = $scope.selectedIdArray.length;
            $scope.numberOfCompletionOfSelectedArray = 0;
            $scope.loading = true;
            $scope.arrayCFCreditTest = {};
            $scope.arrayCFDebitTest = {};
            $scope.arrayCFReversalTest = {};
            
            angular.forEach($scope.selectedIdArray, function (value, key) {
                $scope.arrayCFCreditTest[value] = {};
                $scope.arrayCFDebitTest[value] = {};
                $scope.arrayCFReversalTest[value] = {};
                $scope.allArrayInBatchVsMerchantFirstTest[value] = {'credit_originated_count_batch':0,'credit_originated_sum_batch':0,'debit_originated_count_batch':0,'debit_originated_sum_batch':0,'reversal_originated_count_batch':0,'reversal_originated_sum_batch':0,'credit_created_count_merchants':0,'credit_created_sum_merchants':0,'debit_created_count_merchants':0,'debit_created_sum_merchants':0,'reversal_created_count_merchants':0,'reversal_created_sum_merchants':0};
                $scope.allArrayInBatchVsMerchantSecondTest[value] = {'credit_originated_count_batch':0,'credit_originated_sum_batch':0,'debit_originated_count_batch':0,'debit_originated_sum_batch':0,'reversal_originated_count_batch':0,'reversal_originated_sum_batch':0,'credit_originated_count_merchants':0,'credit_originated_sum_merchants':0,'debit_originated_count_merchants':0,'debit_originated_sum_merchants':0,'reversal_originated_count_merchants':0,'reversal_originated_sum_merchants':0};
                $scope.allArrayInBatchVsMerchantThirdTest[value] = {'credit_returned_count_batch':0,'credit_returned_sum_batch':0,'debit_returned_count_batch':0,'debit_returned_sum_batch':0,'reversal_returned_count_batch':0,'reversal_returned_sum_batch':0,'credit_returned_count_merchants':0,'credit_returned_sum_merchants':0,'debit_returned_count_merchants':0,'debit_returned_sum_merchants':0,'reversal_returned_count_merchants':0,'reversal_returned_sum_merchants':0};
                $scope.allArrayInBatchVsMerchantFourthTest[value] = {'credit_settled_count_batch':0,'credit_settled_sum_batch':0,'debit_settled_count_batch':0,'debit_settled_sum_batch':0,'reversal_settled_count_batch':0,'reversal_settled_sum_batch':0,'credit_settled_count_merchants':0,'credit_settled_sum_merchants':0,'debit_settled_count_merchants':0,'debit_settled_sum_merchants':0,'reversal_settled_count_merchants':0,'reversal_settled_sum_merchants':0};

                BatchFactory.getBatches($scope.batchParams, value)
                    .then(angular.bind(this, function then() {
                        
                        $scope.batchIdForBatchVsMerchant = value;
                        $scope.responseBatchesById[value] = BatchFactory.batches;
                        calculateTotalCalculationsInBatchesVsMerchants($scope.responseBatchesById[value].payload,value);
                    }));
            });

        };
        
        
        
        function calculateTotalCalculationsInBatchesVsMerchants(allData,id){
            calculateCreditRelatedData(allData.credits,id);
            calculateDebitRelatedData(allData.debits, id);
            calculateReversalRelatedData(allData.reversals,id);
        }
        
        function calculateCreditRelatedData(creditData,id){
            calculateCreditOriginatedCountBatch(creditData,id);
            calculateCreditDataMerchants(creditData.merchants_affected,id);
        }
        
        function calculateCreditOriginatedCountBatch(creditData,id){
            $scope.allArrayInBatchVsMerchantFirstTest[id].credit_originated_count_batch += creditData.originated_count+creditData.returned_count+creditData.settled_count;
            $scope.allArrayInBatchVsMerchantFirstTest[id].credit_originated_sum_batch += creditData.originated_sum+creditData.returned_sum+creditData.settled_sum;
            $scope.allArrayInBatchVsMerchantSecondTest[id].credit_originated_count_batch += creditData.originated_count;
            $scope.allArrayInBatchVsMerchantSecondTest[id].credit_originated_sum_batch += creditData.originated_sum;
            $scope.allArrayInBatchVsMerchantThirdTest[id].credit_returned_count_batch += creditData.returned_count;
            $scope.allArrayInBatchVsMerchantThirdTest[id].credit_returned_sum_batch += creditData.returned_sum;
            $scope.allArrayInBatchVsMerchantFourthTest[id].credit_settled_count_batch += creditData.settled_count;
            $scope.allArrayInBatchVsMerchantFourthTest[id].credit_settled_sum_batch += creditData.settled_sum;
        }
        
        function calculateCreditDataMerchants(allMerchants,id){
            var responseBatchesByMerchant = [];
            angular.forEach(allMerchants, function (merchant_id, key) {
            $scope.arrayCFCreditTest[id][merchant_id] = {'credit_created_count':0,'credit_originated_count':0,
                                                            'credit_returned_count':0,'credit_settled_count':0,
                                                            'credit_created_sum':0,'credit_originated_sum':0,
                                                            'credit_returned_sum':0,'credit_settled_sum':0};
            $scope.batchParams = {};    
            $scope.batchParams.merchant = merchant_id;
            BatchFactory.getBatches($scope.batchParams,$scope.batchIdForBatchVsMerchant)
                    .then(angular.bind(this, function then() {
                        responseBatchesByMerchant = BatchFactory.batches;
                        calculateCreditCreatedCountAndSumMerchants(responseBatchesByMerchant.payload.credits,id,merchant_id);
                        calculateCreditOriginatedCountAndSumMerchants(responseBatchesByMerchant.payload.credits,id,merchant_id);
                    }));
            });
            
        }
        
        function calculateCreditCreatedCountAndSumMerchants(creditDataMerchant,id,merchant_id){
            $scope.allArrayInBatchVsMerchantFirstTest[id].credit_created_count_merchants += creditDataMerchant.created_count;
            $scope.allArrayInBatchVsMerchantFirstTest[id].credit_created_sum_merchants += creditDataMerchant.created_sum;
            
            //CF
            $scope.arrayCFCreditTest[id][merchant_id].credit_created_count = (creditDataMerchant.created_count !== null ? creditDataMerchant.created_count:0); 
            $scope.arrayCFCreditTest[id][merchant_id].credit_created_sum = (creditDataMerchant.created_sum !== null ? creditDataMerchant.created_sum:0);
        }
        
        function calculateCreditOriginatedCountAndSumMerchants(creditDataMerchant,id,merchant_id){
            $scope.allArrayInBatchVsMerchantSecondTest[id].credit_originated_count_merchants += creditDataMerchant.originated_count;
            $scope.allArrayInBatchVsMerchantSecondTest[id].credit_originated_sum_merchants += creditDataMerchant.originated_sum;
            $scope.allArrayInBatchVsMerchantThirdTest[id].credit_returned_count_merchants += creditDataMerchant.returned_count;
            $scope.allArrayInBatchVsMerchantThirdTest[id].credit_returned_sum_merchants += creditDataMerchant.returned_sum;
            $scope.allArrayInBatchVsMerchantFourthTest[id].credit_settled_count_merchants += creditDataMerchant.settled_count;
            $scope.allArrayInBatchVsMerchantFourthTest[id].credit_settled_sum_merchants += creditDataMerchant.settled_sum;
            
            //CF
            $scope.arrayCFCreditTest[id][merchant_id].credit_originated_count = (creditDataMerchant.originated_count != null ? creditDataMerchant.originated_count:0); 
            $scope.arrayCFCreditTest[id][merchant_id].credit_originated_sum = (creditDataMerchant.originated_sum != null ? creditDataMerchant.originated_sum:0);
            $scope.arrayCFCreditTest[id][merchant_id].credit_returned_count = (creditDataMerchant.returned_count != null ? creditDataMerchant.returned_count:0); 
            $scope.arrayCFCreditTest[id][merchant_id].credit_returned_sum = (creditDataMerchant.returned_sum != null ? creditDataMerchant.returned_sum:0);
            $scope.arrayCFCreditTest[id][merchant_id].credit_settled_count = (creditDataMerchant.settled_count != null ? creditDataMerchant.settled_count:0); 
            $scope.arrayCFCreditTest[id][merchant_id].credit_settled_sum = (creditDataMerchant.settled_sum != null ? creditDataMerchant.settled_sum:0);
            
            
//            $scope.arrayCFCreditTest[id][merchant_id].credit_originated_count = creditDataMerchant.originated_count;
//            $scope.arrayCFCreditTest[id][merchant_id].credit_returned_count = creditDataMerchant.returned_count;
//            $scope.arrayCFCreditTest[id][merchant_id].credit_settled_count = creditDataMerchant.settled_count;
//            $scope.arrayCFCreditTest[id][merchant_id].credit_originated_sum = creditDataMerchant.originated_sum;
//            $scope.arrayCFCreditTest[id][merchant_id].credit_returned_sum = creditDataMerchant.returned_sum;
//            $scope.arrayCFCreditTest[id][merchant_id].credit_settled_sum = creditDataMerchant.settled_sum;
        }
        
        
        function calculateDebitRelatedData(debitData,id){
           calculateDebitOriginatedCountBatch(debitData,id);
           calculateDebitDataMerchants(debitData.merchants_affected,id);
        }
        
         function calculateDebitOriginatedCountBatch(debit ,id){
            $scope.allArrayInBatchVsMerchantFirstTest[id].debit_originated_count_batch += debit.originated_count+debit.returned_count+debit.settled_count;
            $scope.allArrayInBatchVsMerchantFirstTest[id].debit_originated_sum_batch += debit.originated_sum+debit.returned_sum+debit.settled_sum;
            $scope.allArrayInBatchVsMerchantSecondTest[id].debit_originated_count_batch += debit.originated_count;
            $scope.allArrayInBatchVsMerchantSecondTest[id].debit_originated_sum_batch += debit.originated_sum;
            $scope.allArrayInBatchVsMerchantThirdTest[id].debit_returned_count_batch += debit.returned_count;
            $scope.allArrayInBatchVsMerchantThirdTest[id].debit_returned_sum_batch += debit.returned_sum;
            $scope.allArrayInBatchVsMerchantFourthTest[id].debit_settled_count_batch += debit.settled_count;
            $scope.allArrayInBatchVsMerchantFourthTest[id].debit_settled_sum_batch += debit.settled_sum;
          
         }
        
        function calculateDebitDataMerchants(allMerchants,id){
            var countOfAllMerchantsAffectedInDebit = allMerchants.length;
            var countOfLoadedMerchantsAffectedInDebit = 0;
            var responseBatchesByMerchant;
            angular.forEach(allMerchants, function (merchant_id, key) {
            $scope.arrayCFDebitTest[id][merchant_id] = {'debit_created_count':0,'debit_originated_count':0,
                                                            'debit_returned_count':0,'debit_settled_count':0,
                                                            'debit_created_sum':0,'debit_originated_sum':0,
                                                            'debit_returned_sum':0,'debit_settled_sum':0};
            $scope.batchParams = {};
            $scope.batchParams.merchant = merchant_id;
            BatchFactory.getBatches($scope.batchParams,$scope.batchIdForBatchVsMerchant)
                    .then(angular.bind(this, function then() {
                        responseBatchesByMerchant = BatchFactory.batches;
                        calculateDebitCreatedCountAndSumMerchants(responseBatchesByMerchant.payload.debits,id,merchant_id);
                        calculateDebitOriginatedCountAndSumMerchants(responseBatchesByMerchant.payload.debits,id,merchant_id);
                        countOfLoadedMerchantsAffectedInDebit++;
                        if(countOfAllMerchantsAffectedInDebit==countOfLoadedMerchantsAffectedInDebit){
                            $scope.numberOfCompletionOfSelectedArray++;
                            if($scope.numberOfCompletionOfSelectedArray==$scope.numberOfCompletionOfSelectedArray){
                                $scope.loadingCompletedOfbatchDetailVsAllMerchantsWithInABatch = true;
                                $scope.loading = false;
                            }
                        }
                    }));
            });
            
        }
        
         function calculateDebitCreatedCountAndSumMerchants(debitDataMerchant,id,merchant_id){
            $scope.allArrayInBatchVsMerchantFirstTest[id].debit_created_count_merchants += debitDataMerchant.created_count;
            $scope.allArrayInBatchVsMerchantFirstTest[id].debit_created_sum_merchants += debitDataMerchant.created_sum;
            
            //CF
            $scope.arrayCFDebitTest[id][merchant_id].debit_created_count = (debitDataMerchant.created_count !== null ? debitDataMerchant.created_count:0); 
            $scope.arrayCFDebitTest[id][merchant_id].debit_created_sum = (debitDataMerchant.created_sum !== null ? debitDataMerchant.created_sum:0);
            
//            $scope.arrayCFDebitTest[id][merchant_id].debit_created_count = debitDataMerchant.created_count;
//            $scope.arrayCFDebitTest[id][merchant_id].debit_created_sum = debitDataMerchant.created_sum;
         }
        
        function calculateDebitOriginatedCountAndSumMerchants(debitDataMerchant,id,merchant_id){
            $scope.allArrayInBatchVsMerchantSecondTest[id].debit_originated_count_merchants += debitDataMerchant.originated_count;
            $scope.allArrayInBatchVsMerchantSecondTest[id].debit_originated_sum_merchants += debitDataMerchant.originated_sum;
            $scope.allArrayInBatchVsMerchantThirdTest[id].debit_returned_count_merchants += debitDataMerchant.returned_count;
            $scope.allArrayInBatchVsMerchantThirdTest[id].debit_returned_sum_merchants += debitDataMerchant.returned_sum;
            $scope.allArrayInBatchVsMerchantFourthTest[id].debit_settled_count_merchants += debitDataMerchant.settled_count;
            $scope.allArrayInBatchVsMerchantFourthTest[id].debit_settled_sum_merchants += debitDataMerchant.settled_sum;
            
            //CF
            $scope.arrayCFDebitTest[id][merchant_id].debit_originated_count = (debitDataMerchant.originated_count != null ? debitDataMerchant.originated_count:0); 
            $scope.arrayCFDebitTest[id][merchant_id].debit_originated_sum = (debitDataMerchant.originated_sum != null ? debitDataMerchant.originated_sum:0);
            $scope.arrayCFDebitTest[id][merchant_id].debit_returned_count = (debitDataMerchant.returned_count != null ? debitDataMerchant.returned_count:0); 
            $scope.arrayCFDebitTest[id][merchant_id].debit_returned_sum = (debitDataMerchant.returned_sum != null ? debitDataMerchant.returned_sum:0);
            $scope.arrayCFDebitTest[id][merchant_id].debit_settled_count = (debitDataMerchant.settled_count != null ? debitDataMerchant.settled_count:0); 
            $scope.arrayCFDebitTest[id][merchant_id].debit_settled_sum = (debitDataMerchant.settled_sum != null ? debitDataMerchant.settled_sum:0);
//            console.log($scope.arrayCFDebitTest[id][merchant_id].debit_returned_sum);
//            $scope.arrayCFDebitTest[id][merchant_id].debit_originated_count = debitDataMerchant.originated_count;
//            $scope.arrayCFDebitTest[id][merchant_id].debit_returned_count = debitDataMerchant.returned_count;
//            $scope.arrayCFDebitTest[id][merchant_id].debit_settled_count = debitDataMerchant.settled_count;
//            $scope.arrayCFDebitTest[id][merchant_id].debit_originated_sum = debitDataMerchant.originated_sum;
//            $scope.arrayCFDebitTest[id][merchant_id].debit_returned_sum = debitDataMerchant.returned_sum;
//            $scope.arrayCFDebitTest[id][merchant_id].debit_settled_sum = debitDataMerchant.settled_sum;
        }
        
        
        function calculateReversalRelatedData(reversalData,id){
            calculateReversalOriginatedCountBatch(reversalData,id);
            calculateReversalDataMerchants(reversalData.merchants_affected,id);
        }
        
        
        function calculateReversalOriginatedCountBatch(reversal,id){
            $scope.allArrayInBatchVsMerchantFirstTest[id].reversal_originated_count_batch += reversal.originated_count+reversal.returned_count+reversal.settled_count;
            $scope.allArrayInBatchVsMerchantFirstTest[id].reversal_originated_sum_batch += reversal.originated_sum+reversal.returned_sum+reversal.settled_sum;
            $scope.allArrayInBatchVsMerchantSecondTest[id].reversal_originated_count_batch += reversal.originated_count;
            $scope.allArrayInBatchVsMerchantSecondTest[id].reversal_originated_sum_batch += reversal.originated_sum;
            $scope.allArrayInBatchVsMerchantThirdTest[id].reversal_returned_count_batch += reversal.returned_count;
            $scope.allArrayInBatchVsMerchantThirdTest[id].reversal_returned_sum_batch += reversal.returned_sum;
            $scope.allArrayInBatchVsMerchantFourthTest[id].reversal_settled_count_batch += reversal.settled_count;
            $scope.allArrayInBatchVsMerchantFourthTest[id].reversal_settled_sum_batch += reversal.settled_sum;
       
        }
        
         function calculateReversalDataMerchants(allMerchants,id){
            var responseBatchesByMerchant;
            angular.forEach(allMerchants, function (merchant_id, key) {
            $scope.batchParams = {};
            $scope.batchParams.merchant = merchant_id
            $scope.arrayCFReversalTest[id][merchant_id] = {'reversal_created_count':0,'reversal_originated_count':0,
                                                            'reversal_returned_count':0,'reversal_settled_count':0,
                                                            'reversal_created_sum':0,'reversal_originated_sum':0,
                                                            'reversal_returned_sum':0,'reversal_settled_sum':0};
            BatchFactory.getBatches($scope.batchParams,$scope.batchIdForBatchVsMerchant)
                    .then(angular.bind(this, function then() {
                        responseBatchesByMerchant = BatchFactory.batches;
                        calculateReversalCreatedCountAndSumMerchants(responseBatchesByMerchant.payload.reversals,id,merchant_id);
                        calculateReversalOriginatedCountAndSumMerchants(responseBatchesByMerchant.payload.reversals,id,merchant_id);
                    }));
            });
        }
        
         function calculateReversalOriginatedCountAndSumMerchants(reversalDataMerchant,id,merchant_id){
            $scope.allArrayInBatchVsMerchantSecondTest[id].reversal_originated_count_merchants += reversalDataMerchant.originated_count;
            $scope.allArrayInBatchVsMerchantSecondTest[id].reversal_originated_sum_merchants += reversalDataMerchant.originated_sum;
            $scope.allArrayInBatchVsMerchantThirdTest[id].reversal_returned_count_merchants += reversalDataMerchant.returned_count;
            $scope.allArrayInBatchVsMerchantThirdTest[id].reversal_returned_sum_merchants += reversalDataMerchant.returned_sum;
            $scope.allArrayInBatchVsMerchantFourthTest[id].reversal_settled_count_merchants += reversalDataMerchant.settled_count;
            $scope.allArrayInBatchVsMerchantFourthTest[id].reversal_settled_sum_merchants += reversalDataMerchant.settled_sum;
        
            //CF
            $scope.arrayCFReversalTest[id][merchant_id].reversal_originated_count = (reversalDataMerchant.originated_count != null ? reversalDataMerchant.originated_count:0); 
            $scope.arrayCFReversalTest[id][merchant_id].reversal_originated_sum = (reversalDataMerchant.originated_sum != null ? reversalDataMerchant.originated_sum:0);
            $scope.arrayCFReversalTest[id][merchant_id].reversal_returned_count = (reversalDataMerchant.returned_count != null ? reversalDataMerchant.returned_count:0); 
            $scope.arrayCFReversalTest[id][merchant_id].reversal_returned_sum = (reversalDataMerchant.returned_sum != null ? reversalDataMerchant.returned_sum:0);
            $scope.arrayCFReversalTest[id][merchant_id].reversal_settled_count = (reversalDataMerchant.settled_count != null ? reversalDataMerchant.settled_count:0); 
            $scope.arrayCFReversalTest[id][merchant_id].reversal_settled_sum = (reversalDataMerchant.settled_sum != null ? reversalDataMerchant.settled_sum:0);
//            $scope.arrayCFReversalTest[id][merchant_id].reversal_originated_count = reversalDataMerchant.originated_count;
//            $scope.arrayCFReversalTest[id][merchant_id].reversal_returned_count = reversalDataMerchant.returned_count;
//            $scope.arrayCFReversalTest[id][merchant_id].reversal_settled_count = reversalDataMerchant.settled_count;
//            $scope.arrayCFReversalTest[id][merchant_id].reversal_originated_sum = reversalDataMerchant.originated_sum;
//            $scope.arrayCFReversalTest[id][merchant_id].reversal_returned_sum = reversalDataMerchant.returned_sum;
//            $scope.arrayCFReversalTest[id][merchant_id].reversal_settled_sum = reversalDataMerchant.settled_sum;
         }
        
         function calculateReversalCreatedCountAndSumMerchants(reversalDataMerchant,id,merchant_id){
            $scope.allArrayInBatchVsMerchantFirstTest[id].reversal_created_count_merchants += reversalDataMerchant.created_count;
            $scope.allArrayInBatchVsMerchantFirstTest[id].reversal_created_sum_merchants += reversalDataMerchant.created_sum;
            //CF
            $scope.arrayCFReversalTest[id][merchant_id].reversal_created_count = (reversalDataMerchant.created_count !== null ? reversalDataMerchant.created_count:0); 
            $scope.arrayCFReversalTest[id][merchant_id].reversal_created_sum = (reversalDataMerchant.created_sum !== null ? reversalDataMerchant.created_sum:0);
            
//            $scope.arrayCFReversalTest[id][merchant_id].reversal_created_count = reversalDataMerchant.created_count;
//            $scope.arrayCFReversalTest[id][merchant_id].reversal_created_sum = reversalDataMerchant.created_sum;
            
         }
        
        
        $scope.calculateTransactionIdDifferenceAndCreatedCount = function (id, calculation_data) {
            $scope.a = [];
            $scope.status = 'FAILED';
            $scope.batch_id = id;
            $scope.txnIdDifference = calculation_data.txn_id_end - calculation_data.txn_id_start + 1;
            $scope.createdCount = calculation_data.debits.created_count + calculation_data.credits.created_count +
                calculation_data.reversals.created_count + calculation_data.errors.created_count +
                calculation_data.declines.created_count + calculation_data.voids.created_count;
            $scope.difference = Math.abs($scope.txnIdDifference - $scope.createdCount);
            if ($scope.difference === 0) {
                $scope.status = 'PASSED';
            }
            $scope.a.push($scope.batch_id, $scope.txnIdDifference, $scope.createdCount, $scope.difference, $scope.status);
            $scope.createdCountTxnIdArray.push($scope.a);


        };
        $scope.calculateCreditsDifference = function (id, calculation_data) {
            $scope.b = [];
            $scope.creditStatus = 'FAILED';
            $scope.batch_id = id;
            $scope.credit_created_count = round(calculation_data.created_count, 2);
            $scope.credit_originated_count = round(calculation_data.originated_count + calculation_data.settled_count + calculation_data.returned_count, 2);
            $scope.creditCountDifference = Math.abs($scope.credit_created_count - $scope.credit_originated_count);
            $scope.credit_created_sum = round(calculation_data.created_sum, 2);
            $scope.credit_originated_sum = round(calculation_data.originated_sum + calculation_data.settled_sum + calculation_data.returned_sum, 2);
            $scope.creditSumDifference = Math.abs($scope.credit_created_sum - $scope.credit_originated_sum);

            if ($scope.creditCountDifference == 0 && $scope.creditSumDifference == 0) {
                $scope.creditStatus = 'PASSED';
            }


            $scope.b.push($scope.batch_id,
                $scope.credit_created_count,
                $scope.credit_originated_count,
                $scope.creditCountDifference,
                $scope.credit_created_sum,
                $scope.credit_originated_sum,
                $scope.creditSumDifference,
                $scope.creditStatus);

            $scope.creditArray.push($scope.b);

        };
        $scope.calculateDebitsDifference = function (id, calculation_data) {
            $scope.c = [];
            $scope.debitStatus = 'FAILED';
            $scope.batch_id = id;
            $scope.debit_created_count = round(calculation_data.created_count, 2);
            $scope.debit_originated_count = round(calculation_data.originated_count + calculation_data.settled_count + calculation_data.returned_count, 2);
            $scope.debitCountDifference = Math.abs($scope.debit_created_count - $scope.debit_originated_count);

            $scope.debit_created_sum = round(calculation_data.created_sum, 2);
            $scope.debit_originated_sum = round(calculation_data.originated_sum + calculation_data.settled_sum + calculation_data.returned_sum, 2);
            $scope.debitSumDifference = Math.abs($scope.debit_created_sum - $scope.debit_originated_sum);
            if ($scope.debitCountDifference === 0 && $scope.debitSumDifference === 0) {
                $scope.debitStatus = 'PASSED';
            }

            $scope.c.push($scope.batch_id,
                $scope.debit_created_count,
                $scope.debit_originated_count,
                $scope.debitCountDifference,
                $scope.debit_created_sum,
                $scope.debit_originated_sum,
                $scope.debitSumDifference,
                $scope.debitStatus);

            $scope.debitArray.push($scope.c);
        };
        $scope.calculateReversalsDifference = function (id, calculation_data) {
            $scope.d = [];
            $scope.reversalStatus = 'FAILED';
            $scope.batch_id = id;
            $scope.reversal_created_count = round(calculation_data.created_count, 2);
            $scope.reversal_originated_count = round(calculation_data.originated_count + calculation_data.settled_count + calculation_data.returned_count, 2);
            $scope.reversalCountDifference = Math.abs($scope.reversal_created_count - $scope.reversal_originated_count);

            $scope.reversal_created_sum = round(calculation_data.created_sum, 2);
            $scope.reversal_originated_sum = round(calculation_data.originated_sum + calculation_data.settled_sum + calculation_data.returned_sum, 2);
            $scope.reversalSumDifference = Math.abs($scope.reversal_created_sum - $scope.reversal_originated_sum);
            if ($scope.reversalCountDifference === 0 && $scope.reversalSumDifference === 0) {
                $scope.reversalStatus = 'PASSED';
            }

            $scope.d.push($scope.batch_id,
                $scope.reversal_created_count,
                $scope.reversal_originated_count,
                $scope.reversalCountDifference,
                $scope.reversal_created_sum,
                $scope.reversal_originated_sum,
                $scope.reversalSumDifference,
                $scope.reversalStatus);

            $scope.reversalArray.push($scope.d);

        };
        /*
         * fetches monthly merchant activity using date range and merchant id
         */
        $scope.fetchMonthlyMerchantActivityAndBatches = function () {
            $scope.merchantActivity = [];
            $scope.responseBatchesByDateForMerchantActivity = [];
            $scope.monthlyMerchantParams = {};
            $scope.totalMerchantActivity = {};

            $scope.totalMerchantActivity = {count: 0, sum: 0};
            $scope.totalBatches = {count: 0, sum: 0};

            $scope.array1 = {};

            $scope.loading = true;
            $scope.loadedMerchantCount = 0;
            $scope.loadTableBatchVsMonthlyMerchantActivity = false;


            $scope.merchantIdArray = $scope.merchant_id.split(',');

            angular.forEach($scope.merchantIdArray, function (eachMerchantId, keyMerchantId) {
                eachMerchantId = eachMerchantId.trim();
                $scope.array1[eachMerchantId] = {};
                var end_date = new moment().endOf('month').format("YYYY-MM-DD");
                var start_date = new moment().subtract(1, 'months').startOf('month').format('YYYY-MM-DD');
                $scope.monthlyMerchantParams.start_date = start_date;
                $scope.monthlyMerchantParams.end_date = end_date;

                MerchantActivityFactory.getMerchantMonthlyActivity($scope.monthlyMerchantParams, eachMerchantId)
                    .then(angular.bind(this, function then() {
                        $scope.merchantActivity = MerchantActivityFactory.merchantActivityReports;
                        $scope.merchantActivity = $scope.merchantActivity.payload;
                        calculateTotalCountInMerchantActivity($scope.merchantActivity, eachMerchantId);
                    }));

                BatchFactory.getBatchesByDate($scope.monthlyMerchantParams)
                    .then(angular.bind(this, function then() {
                        $scope.responseBatchesByDateForMerchantActivity = BatchFactory.batches;
                        calculateTotalInBatches($scope.responseBatchesByDateForMerchantActivity.payload, eachMerchantId);
                    }));
            });

        };
        function calculateTotalCountInMerchantActivity(monthlyMerchantReport, merchantId) {

            angular.forEach(monthlyMerchantReport, function (value, key) {
                $scope.totalMerchantActivity.count += value.txn_count;
                $scope.totalMerchantActivity.sum += value.txn_sum;
            });

            $scope.array1[merchantId]['merchant'] = $scope.totalMerchantActivity;
            $scope.loadIncompleteOfMonthlyMerchantActivity = false;

        }

        function calculateTotalInBatches(batches, merchantId) {
            var batchParams = {};
            var responseBatchesById = [];
            var response = {};
            var loadedBatchCountOfMerchant = 0;
            $scope.loadTableBatchVsMonthlyMerchantActivity = false;

            angular.forEach(batches, function (value, key) {
                batchParams.merchant = merchantId;
                $scope.id = value.id;
                BatchFactory.getBatches(batchParams, value.id)
                    .then(angular.bind(this, function then() {
                        responseBatchesById = BatchFactory.batches;
                        response = responseBatchesById.payload;
                        $scope.totalBatches.count += response.debits.created_count + response.credits.created_count + response.reversals.created_count + response.returns.created_count + response.errors.created_count + response.declines.created_count + response.voids.created_count;
                        $scope.totalBatches.sum += response.debits.created_sum + response.credits.created_sum + response.reversals.created_sum + response.returns.created_sum + response.errors.created_sum + response.declines.created_sum + response.voids.created_sum;

                        loadedBatchCountOfMerchant += 1;
                        if (loadedBatchCountOfMerchant == Object.keys(batches).length) {
                            $scope.loadIncompleteOfAllMerchantTotalBatchCount = false;
                            if ($scope.loadIncompleteOfMonthlyMerchantActivity == false && $scope.loadIncompleteOfAllMerchantTotalBatchCount == false) {
                                $scope.loadIncompleteOfAllMerchantTotalBatchCount = true;
                                $scope.loadedMerchantCount += 1;
                                if ($scope.loadedMerchantCount == $scope.merchantIdArray.length) {
                                    $scope.loading = false;
                                    $scope.loadTableBatchVsMonthlyMerchantActivity = true;
                                }
                            }
                        }
                    }));

            });
            $scope.array1[merchantId]['batch'] = $scope.totalBatches;
//            $scope.loadingCheckerMerchantMonthlyVsBatches = true;
        }


        $scope.fetchIsoMonthlyActivityVsBatches = function () {
            $scope.monthlyIsoParams = {};
            $scope.isoIdArray = $scope.iso.split(',');
            $scope.loading = true;
            $scope.loadIsoCount = 0;
            $scope.loadTableIsoMonthlyVsBatches = false;
            $scope.totalIsoMonthlyActivity = {count: 0, sum: 0};
            $scope.totalBatchesForIso = {count: 0, sum: 0};

            $scope.arrayOfIsoMonthlyActivity = {};

            var end_date = new moment().endOf('month').format("YYYY-MM-DD");
            var start_date = new moment().subtract(1, 'months').startOf('month').format('YYYY-MM-DD');
            $scope.monthlyIsoParams.start_date = start_date;
            $scope.monthlyIsoParams.end_date = end_date;

            angular.forEach($scope.isoIdArray, function (eachIsoId, keyIsoId) {
                eachIsoId = eachIsoId.trim();
                $scope.arrayOfIsoMonthlyActivity[eachIsoId] = {};

                IsoActivityFactory.getIsoMonthlyActivity($scope.monthlyIsoParams, eachIsoId)
                    .then(angular.bind(this, function then() {
                        $scope.isoMonthlyActivity = IsoActivityFactory.isoActivityReports.payload;
                        calculateTotalCountInIsoMonthlyActivity($scope.isoMonthlyActivity, eachIsoId);

                    }));

                BatchFactory.getBatchesByDate($scope.monthlyIsoParams)
                    .then(angular.bind(this, function then() {
                        $scope.responseBatchesByDateForIsoMonthlyActivity = BatchFactory.batches.payload;
                        calculateTotalInBatchesForISo($scope.responseBatchesByDateForIsoMonthlyActivity, eachIsoId);
                    }));


            });


        };

        function calculateTotalCountInIsoMonthlyActivity(monthlyIsoReport, isoId) {
            angular.forEach(monthlyIsoReport, function (value, key) {
                $scope.totalIsoMonthlyActivity.count += value.txn_count;
                $scope.totalIsoMonthlyActivity.sum += value.txn_sum;
            });
            $scope.arrayOfIsoMonthlyActivity[isoId]['iso'] = $scope.totalIsoMonthlyActivity;
            $scope.loadInCompleteOfIsoMonthlyActivity = false;
        }

        function calculateTotalInBatchesForISo(batches, isoId) {
            var isoParams = {};
            var responseBatchesById = [];
            var response = {};
            var loadCount = 0;
            $scope.loadTableIsoMonthlyVsBatches = false;

            angular.forEach(batches, function (value, key) {
                isoParams.iso = isoId;
                BatchFactory.getBatches(isoParams, value.id)
                    .then(angular.bind(this, function then() {
                        responseBatchesById = BatchFactory.batches;
                        response = responseBatchesById.payload;
                        $scope.totalBatchesForIso.count += response.debits.created_count + response.credits.created_count + response.reversals.created_count + response.returns.created_count + response.errors.created_count + response.declines.created_count + response.voids.created_count;
                        $scope.totalBatchesForIso.sum += response.debits.created_sum + response.credits.created_sum + response.reversals.created_sum + response.returns.created_sum + response.errors.created_sum + response.declines.created_sum + response.voids.created_sum;
                        loadCount += 1;
                        if (loadCount == Object.keys(batches).length) {
                            $scope.loadInCompleteOfAllIsoTotalBatchCount = false;
                            if ($scope.loadInCompleteOfIsoMonthlyActivity == false && $scope.loadInCompleteOfAllIsoTotalBatchCount == false) {
                                $scope.loadInCompleteOfAllIsoTotalBatchCount = true;
                                $scope.loadIsoCount += 1;
                                if ($scope.loadIsoCount == $scope.isoIdArray.length) {
                                    $scope.loading = false;
                                    $scope.loadTableIsoMonthlyVsBatches = true;
                                }
                                $scope.loadTableIsoMonthlyVsBatches = true;
                            }
                        }
                    }));
            });
            $scope.arrayOfIsoMonthlyActivity[isoId]['batch'] = $scope.totalBatchesForIso;
        }

        function round(value, decimals) {
            return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
        }
        $scope.roundOff = function(value,decimals){
            return Number(Math.round(value + 'e' + decimals) + 'e-' + decimals);
        };
        
        
//        openModal(key,arrayCFCreditTest,'credit_originated_count')
        
        $scope.openModal = function(batch_id,array,array_detail_type){
            $scope.modal_batch = batch_id;
            $scope.modal_array = array;
            $scope.modal_array_detail_type = array_detail_type;
            $scope.arrayForModal = [];
            $scope.firstkey = Object.keys($scope.modal_array)[0];
//            console.log(Object.keys(array).length);
            $scope.calculatedCFCount=0;
            $scope.loadingTableCompleted=false;
            $scope.loading = true;
            $scope.finalArray = [];
            $scope.CF = 0;
            angular.forEach($scope.modal_array,function (datas, key){
                $scope.items={id:0,value:0,cf:0};
                $scope.items.id = key;
                $scope.items.value = $scope.roundOff(datas[array_detail_type],2);
                $scope.items.cf = $scope.CF+$scope.roundOff($scope.items.value,2);
                $scope.CF = $scope.items.cf;
                $scope.finalArray.push($scope.items);
                $scope.calculatedCFCount++;
            });
            if($scope.calculatedCFCount===Object.keys(array).length){
                $scope.loadingTableCompleted=true;
                $scope.loading=false;
            }
            
//            console.log("PREVIOUS LENGTH:->"+$scope.objArr.length);
            
            $scope.objArr.length=0;
//            console.log($scope.objArr);
        };
        $scope.objArr=[];
        $scope.cumulative = [];
        $scope.cumulative[-1] = 0;
        
        $scope.print = function(key){
            $scope.keyyy=key;
            console.log($scope.keyyy);
        }
        
//        $scope.getCumulativeSum=function(count,value){
//            
//            if(value === null){
//                value=0;
//            }
//            
//            if(count === 0) {
//                $scope.cumulative.length = 0;
//                $scope.cumulative[-1] = 0;
//            }
//            
//            console.log($scope.roundOff(value,2));
//            console.log(count);
//            console.log('break');
//            $scope.cumulative[count] = $scope.roundOff($scope.cumulative[count-1]) + $scope.roundOff(value,2);
//            return $scope.cumulative[count];
//        };
        
        

    }

    CheckersController.$inject = ['$scope', 'BatchFactory', 'MerchantActivityFactory', 'IsoActivityFactory', '$stateParams', '$filter', '$timeout'];
    angular.module('evericheck').controller('CheckersController', CheckersController);
})();